


//http://maps.googleapis.com/maps/api/geocode/json?address=22032&sensor=true
	var UserLocation = {};



function zipLookUpViaGoogleMaps(zipCode) {

	if(typeof zipCode == "undefined") {
		zipCode = 22032;
	}

	UserLocation._zip = zipCode;

	$.getJSON("http://maps.googleapis.com/maps/api/geocode/json?address="+zipCode+"&sensor=false", function(data){
		console.log(data);
		var addressOffset = data.results[0].address_components.length;
		UserLocation._state = data.results[0].address_components[addressOffset-2].short_name;
		UserLocation._state_full = data.results[0].address_components[addressOffset-2].long_name;
		UserLocation._city = data.results[0].address_components[addressOffset-3].long_name;		
		updateLocationFields();
	});
}

function zipLookUpViaZiptastic(zipCode) {

	if(typeof zipCode == "undefined") {
		zipCode = 22032;
	}

	UserLocation._zip = zipCode;

	$.getJSON("http://zip.getziptastic.com/v2/US/"+zipCode, function(data){
		console.log(data.city);
		
		UserLocation._state = data.state_short;
		UserLocation._state_full = data.state;
		UserLocation._city = data.city;
		updateLocationFields();
		
	});
}




function updateLocationFields() {
		$("label[for='zip']").html(UserLocation._state);
		$("select.electric_utility.show").removeClass("show validate-me");
		$("#electric_utility-"+UserLocation._state).addClass("show validate-me");
		$("input#city").val(UserLocation._city);
		$("input#state").val(UserLocation._state);
		console.log("input city: "+$("input#city").val()+" | input state: "+$("input#state").val());
}




function build_getUtilities() {

	var LocationData = {};

	$("#zip").on("change",function(){

		var inputZip = $(this).val();

		if (inputZip.length === 5) {
			zipLookUpViaZiptastic(inputZip);
		}

	});


}


