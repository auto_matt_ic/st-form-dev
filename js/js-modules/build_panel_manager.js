
var TheForm = {panel:{}, _isMobile:false};

TheForm.panel._padding = parseInt($("fieldset.form-panel").css("padding-left"))*2;
TheForm.panel._width = $("fieldset.form-panel").width()+TheForm.panel._padding;
TheForm.panel._count = 4;
TheForm.panel._current = 0;
TheForm.panel._offset_values = [];
//TheForm.panel._totalFields = "";
//TheForm.panel._validFields = "";

function activateLabelAnimation() {


	$("input[type='text'],input[type='email'],input[type='number']").each(function(){

		var input_id = $(this).attr("id");

		$(this).on("focus",function(){
			$("label[for="+input_id+"]").addClass("show")
		});
		$(this).on("change",function(){
			$("label[for="+input_id+"]").addClass("show")
			//alert("shit just got real");
		});

	});

}


function navToPanel(panel_id) {

	if(typeof panel_id == "undefined") {
		var panel_id = Math.floor(Math.random()*4);
	}

	$(".form__panel-wrap").css({"left":(0-TheForm.panel._offset_values[panel_id])+"px"});

	$("[data-currentstep]").attr("data-currentstep",panel_id);

	TheForm.panel._current = panel_id;
}

function nextPanel() {

	if ((TheForm.panel._current+1)<3) {
		navToPanel(TheForm.panel._current + 1);
	}
}

function prevPanel() {
	if ((TheForm.panel._current-1)>-1) {
		navToPanel(TheForm.panel._current - 1);
	}
}

function autoForward(panel_id) {

	setTimeout(function(){

		nextPanel()
		//$("a.prompt__autoforward").removeClass("show");


	},2300);

}




function activateDirectionalNav() {
	$(".next-panel").click(function(){
		nextPanel();
	});
	$(".prev-panel").click(function(){
		prevPanel();
	});
}

function isElementInCurrentPanel(element) {

	var elementStep = $(element).parents("fieldset").attr("data-step");
	var parent_container = $(element).parents('fieldset');

	return (elementStep == TheForm.panel._current);
}

function getElementPanelID(element) {

	var elementStep = $(element).parents("fieldset").attr("data-step");


	return
}




function activatePanelNav() {




	$("a.step-button").each(function(){

		var destinationStep = $(this).attr("data-step"); // pull step value from attribute on a.step-button
		var page_offset = (parseFloat(destinationStep)*parseFloat(TheForm.panel._width));
		TheForm.panel._offset_values[destinationStep] = page_offset;
		var carat_position = $(this).position().left;


		$(this).click(function(){

				$(".form__panel-wrap").css({"left":(0-page_offset)+"px"});
				$("[data-currentstep]").attr("data-currentstep",destinationStep);
				TheForm.panel._current = destinationStep;
				//console.log("page offset: "+page_offset+" | step_number: "+destinationStep+" | page_width: "+TheForm.panel._width);


		});

	});


}



function build_panelManager() {
	activatePanelNav();
	activateLabelAnimation();

}