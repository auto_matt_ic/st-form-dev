

function disableEnterButton() {
    $('.form-panel__0,.form-panel__1,.form-panel__2').bind('keypress', function(event)
        {
       if(event.keyCode == 9)
       {
        event.preventDefault();
          console.log("enter or tab pressed");
        //  return false;

       }
    });
}

//11000




function isEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

function isZip(zipcode) {

    return /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipcode);
}

function isPhone(phoneNumber) {
    //return /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/.test(phoneNumber);
    return phoneNumber.length>0;
}


function isText(inputText) {
  return inputText.length>0
}

function isSelect(selectValue) {
    return selectValue.length>0
}

function resetRadioButtons() {
  $("input:radio").each(function(){
    $(this).attr("data-fieldStatus","isNotValid");
  });
}


function validateMyField(myField) {

        var inputValue = $(myField).val();
        var dataType = $(myField).attr("data-validate");
        var validationStatus = "null";

        //remove previous validation flags
        $(myField).siblings(".validationFlag").remove();



        switch(dataType) {
           case "select" :
              validationStatus = isSelect(inputValue);
            break;
            case "text" :
              validationStatus = isText(inputValue);
             break;
            case "zipcode":
               validationStatus = isZip(inputValue);
            break;
            case "phone":
                 validationStatus = isPhone(inputValue);
            break;
            case "email":
                 validationStatus = isEmail(inputValue);
            break;

        }

        if($(myField).attr("type") == "radio") {

          resetRadioButtons();
           validationStatus = ($(myField).filter(":checked").val().length > 0);

         }


        // think of a way to clean this up


        if(validationStatus == true) {
         $(myField).before("<div class='validationFlag'></div>");
         $(myField).attr("data-fieldStatus","isValid");
          $(myField).siblings(".validationFlag").addClass("isValid");
        } else {
            //add false flag
             $(myField).before("<div class='validationFlag'></div>");
             $(myField).attr("data-fieldStatus","isNotValid");
             $(myField).siblings(".validationFlag").addClass("isNotValid");
        }
}





function isFieldsetValid(fieldset) {

  var totalFields = parseInt($(fieldset).find(".validate-me").length);
  var totalValidFields = parseInt($(fieldset).find("[data-fieldStatus='isValid']").length);

  var radioFields = parseInt($(fieldset).find("[type='radio']").length);

  if (radioFields > 0) {

    totalFields = totalFields - 1;
    totalValidFields = totalValidFields;

  }


   if (totalFields != totalValidFields) {
   //console.log("there are currently "+totalFields-totalValidFields+" invalid fields");
   }

   $(fieldset).attr("data-validFields",totalValidFields);
   $(fieldset).attr("data-totalFields",totalFields);
   $(fieldset).attr("data-invalidFields",totalFields-totalValidFields);
   $(fieldset).attr("data-isFieldsetValid", (totalValidFields == totalFields));

   $(".nav__quote-form ul li[data-step='"+TheForm.panel._current+"'] .validation-indicator").html(totalValidFields);

   if (totalFields == totalValidFields) {
      $(".nav__quote-form ul li[data-step='"+TheForm.panel._current+"'] .validation-indicator").addClass("isValid");
   }

 return (totalFields == totalValidFields);

}



function build_formMasks() {
        $("#phone_home").mask("(000) 000-0000");
}


function autoForwardOnFieldsetValidation(element) {



   $(element).each(function(){


         var thisElement = $(this);
         var parentFieldset = $(this).parents("fieldset");




      $(this).change(function(){

         var isMyFieldsetValid =  isFieldsetValid($(parentFieldset));

         //console.log("is element in current panel: "+isElementInCurrentPanel($(thisElement)));
         //console.log("is my fieldset valid: "+isMyFieldsetValid);

        if (isElementInCurrentPanel($(thisElement)) && isMyFieldsetValid) {
          $(parentFieldset).find(".loading-bar").addClass("play");
          autoForward();

        }

      });

  });

}


function activateCircleRadioButtons() {
  $("input:radio").each(function(){

    var input_id = $(this).attr("id");
    $(this).on("click",function(){
        $("label.radio-circles").removeClass("selected");

        $("input:radio").attr("data-fieldStatus","isNotValid");

        $("label[for="+input_id+"]").addClass("selected");
        $(this).attr("data-fieldStatus","isValid");
    });
  })
}



function build_formValidation() {

    disableEnterButton();
    activateCircleRadioButtons();


    $('input.validate-me, select').on("change", function() {
        validateMyField($(this));

    })

    $("fieldset").focus(function(){
      var thisPanel = $(this).parents("fieldset").attr("data-step");
      //navToPanel(thisPanel);
    });




    autoForwardOnFieldsetValidation($("input.validate-me, select"));


}

