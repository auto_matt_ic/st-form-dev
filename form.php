   <!--  -->
    			<form class="form__solar-quote" action="//receiver.ceeleads.info/leads/post2/" data-currentstep="0" id="solar-quote" method="post">
                  
                 <div class="form__panel-wrap" >
    				<fieldset class="form-panel form-panel__0" tabindex="0" id="" data-step="0">

                        <h2>Let's Get Started!</h2>

                        <ul class="form-panel__field-layout">

                            <li>
                                <label class="displacement-effect" for="zip"  id="label-zip">Zip Code</label>
                                <input type="text" type="number" id="zip" maxlength="5" name="zip" data-validate="zipcode" class="validate-me" />
                            </li>

                            <!-- pattern="[0-9]*" -->

                            <li>
                                <label for="electric_bill" id="label-electric_bill">
                                    Monthly Electric Bill:
                                </label>
                                <select name="electric_bill" id="electric_bill" data-validate="select" class="validate-me" data-validate="select" >
                                    <option value=""></option>
                                     <option value="$0-50">$0-50</option>
                                     <option value="$51-100">$51-100</option>
                                     <option value="$101-150">$101-150</option>
                                     <option value="$151-200">$151-200</option>
                                     <option value="$201-300">$201-300</option>
                                     <option value="$301-400">$301-400</option>
                                     <option value="$401-500">$401-500</option>
                                     <option value="$501-600">$501-600</option>
                                     <option value="$601-700">$601-700</option>
                                     <option value="$701-800">$701-800</option>
                                     <option value="$801+">$801+</option>
                                 </select>
                            </li>
                            <li>
                                <label class="" >
                                    Electric Provider
                                </label>
                               <!--   <select name="electric_bill" id="electric_bill">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>

                                </select> -->

                                <?php include 'utility_select.php' ?>


                            </li>

                        </ul>

                        <div class="loading-bar"></div>
       				</fieldset><fieldset tabindex="0" class="form-panel form-panel__1" id="" data-step="1">

                      <h2>My Home</h2>

                      <ul class="form-panel__field-layout">
                            <li>
                                <label class="displacement-effect" for="first_name" id="label-first_name">First Name</label>

                                <input class="validate-me" type="text" id="first_name" name="first_name" data-validate="text"  />

                            </li>
                            <li>
                                 <label class="displacement-effect" for="last_name" id="label-first_name">Last Name</label>

                                  <input class="validate-me"  type="text" id="last_name" name="last_name" data-validate="text" />

                            </li>
                            <li class="question question__property-ownership">
                                <label class="" id="label-property_ownership">Do You Own Your Home?</label>


                                <label  class="label__property_ownership radio-circles auto-forward" id="label-property_ownership-YES" for="property_ownership-YES">
                                    YES
                                    <input class="radio-circles validate-me" id="property_ownership-YES" name="property_ownership" type="radio" value="OWN" />
                                </label>

                                <label class="label__property_ownership radio-circles" id="label-property_ownership-NO" for="property_ownership-NO" >
                                    NO
                                  <input class="radio-circles validate-me" id="property_ownership-NO" name="property_ownership" type="radio" value="RENT" />
                                </label>


                            </li>



                        </ul>


                        <div class="loading-bar"></div>
    				</fieldset><fieldset tabindex="0" class="form-panel form-panel__2" id="" data-step="2" >
                      <h2>My Contact Preferences</h2>
    					<ul class="form-panel__field-layout">
    						<li>
    							  <label class="displacement-effect" for="street" id="label-street">Street Address</label>
                                  <input class="validate-me" type="text" id="street" name="street" data-validate="text"  />

    						</li>
    						<li>
    							<label class="displacement-effect" for="email" id="label-email">Email</label>
                                <input  class="validate-me" type="email" id="email" name="email" data-validate="email" />
                                   <br><a class="randomEmail">random</a>

    						</li>
    						<li>
    							<label class="displacement-effect" for="phone_home" id="label-phone">Phone</label>
                                <input  class="validate-me" type="text" id="phone_home" name="phone_home" data-validate="phone" />
    						</li>
                            <li>
                                <input type="submit" value="submit" id="quote-submit" >

                            </li>
    					</ul>
                         <p class="small-text">By clicking above, you agree to the Privacy Policy and authorize Solar America and up to four Solar Companies to call or send pre-recorded or text messages to the telephone number that you entered above using automated telephone technology, even if your telephone number is currently listed on any any state, federal or corporate “Do Not Call” list, and you are not required to give your consent as a condition of any purchase.</p>
    				</fieldset>




                </div><!-- ./ form__panel-wrap -->

                 <div class="submit-status submit-status__success">
              <img src="images/submit-success.png">
              <h3>
                Success!
              </h3>
              <p>
                Your information has been succesfully submitted. You will be contacted shortly by 2 or 3 solar installation representatives. 
              </p>
               <p class="submit-status__text">
                
              </p>
              
            </div>
             <div class="submit-status submit-status__fail">

               <img src="images/submit-fail.png">
              <h3>
                Oooops 
              </h3>
              <p>
               Something got mixed up
              </p>
              <p class="submit-status__text">
                
              </p>
              <p>
                <a class="restart">restart</a>

              </p>
            </div>



                <!-- === hidden fields ==== -->

                    <input id="ret_url" name="ret_url" type="hidden" value="/thanks-srxx/131213a/?campid=7DB672BB80F7CF60" />
                    <input id="solar_electric" name="solar_electric" type="hidden" value="true" />
                    <input id="electric_utility" name="electric_utility" type="hidden" value="" />
                    <input id="city" name="city" type="hidden" value="" />
                    <input id="state" name="state" type="hidden" value="" />
                     <input id="campid" name="campid" type="hidden" value="7DB672BB80F7CF60" />


                <!-- === end hidden fields ==== -->


    			</form>

    			<nav class="nav__quote-form" data-currentstep="0">

    				<ul class="">
                        <li class="progress-bar"

                        </li>
    					<li class="step step__0" data-step="0" >
    							<a class="step-button step-button__0" data-step="0"></a>
                                <span class="validation-indicator" data-validfields="0">0</span>
                                <p class="action-label">My Electric Bill</p>
    					</li><li class="step step__1"  data-step="1" >
    							<a class="step-button step-button__1" data-step="1"></a>
                                <span class="validation-indicator" data-validfields="0">0</span>
                                 <p class="action-label">My Home</p>
    					</li><li class="step step__2" data-step="2"  >
    							<a class="step-button step-button__2" data-step="2"></a>
                                <span class="validation-indicator" data-validfields="0">0</span>
                                 <p class="action-label">My Contact Preferences</p>
    					</li>
    				</ul>
    			</nav>