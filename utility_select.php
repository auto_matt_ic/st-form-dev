
                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-AK">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Alaska Village Electric Coop">
                            Alaska Village Electric Coop
                        </option>

                        <option value=
                        "Anchorage Municipal Light & Power (ML&P)">
                            Anchorage Municipal Light & Power (ML&P)
                        </option>

                        <option value="Chugach Electric Association">
                            Chugach Electric Association
                        </option>

                        <option value="Copper Valley Electric Association">
                            Copper Valley Electric Association
                        </option>

                        <option value=
                        "Golden Valley Electric Association (GVEA)">
                            Golden Valley Electric Association (GVEA)
                        </option>

                        <option value="Homer Electric Association">
                            Homer Electric Association
                        </option>

                        <option value="Ketchikan Public Utilities (KPU)">
                            Ketchikan Public Utilities (KPU)
                        </option>

                        <option value="Kodiak Electric Association">
                            Kodiak Electric Association
                        </option>

                        <option value="Matanuska Electric Association">
                            Matanuska Electric Association
                        </option>

                        <option value="Sitka">
                            Sitka
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-AL">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Alabama Power Co">
                            Alabama Power Co
                        </option>

                        <option value="Andalusia">
                            Andalusia
                        </option>

                        <option value="Arab Electric Coop">
                            Arab Electric Coop
                        </option>

                        <option value="Athens Utilities">
                            Athens Utilities
                        </option>

                        <option value="Baldwin EMC">
                            Baldwin EMC
                        </option>

                        <option value="Black Warrior EMC">
                            Black Warrior EMC
                        </option>

                        <option value="Central Alabama Electric Coop (CAEC)">
                            Central Alabama Electric Coop (CAEC)
                        </option>

                        <option value="Cherokee Electric Coop">
                            Cherokee Electric Coop
                        </option>

                        <option value="Covington Electric Coop">
                            Covington Electric Coop
                        </option>

                        <option value="Cullman Electric Coop (Cullman EC)">
                            Cullman Electric Coop (Cullman EC)
                        </option>

                        <option value="Decatur Utilities">
                            Decatur Utilities
                        </option>

                        <option value="Direct Energy">
                            Direct Energy
                        </option>

                        <option value="Dixie Electric Coop (Dixie EC)">
                            Dixie Electric Coop (Dixie EC)
                        </option>

                        <option value="Dothan">
                            Dothan
                        </option>

                        <option value="Fairhope">
                            Fairhope
                        </option>

                        <option value="Florence">
                            Florence
                        </option>

                        <option value=
                        "Fort Payne Improvement Authority (FPIA)">
                            Fort Payne Improvement Authority (FPIA)
                        </option>

                        <option value="Hunstville Utilities (HSVUTIL)">
                            Hunstville Utilities (HSVUTIL)
                        </option>

                        <option value="Joe-Wheeler EMC (JWEMC)">
                            Joe-Wheeler EMC (JWEMC)
                        </option>

                        <option value="North Alabama Electric Coop">
                            North Alabama Electric Coop
                        </option>

                        <option value="Pioneer Electric Coop">
                            Pioneer Electric Coop
                        </option>

                        <option value="PowerSouth Energy Coop">
                            PowerSouth Energy Coop
                        </option>

                        <option value="Riviera Utilities">
                            Riviera Utilities
                        </option>

                        <option value="Sand Mountain Electric Coop (SMEC)">
                            Sand Mountain Electric Coop (SMEC)
                        </option>

                        <option value="Sheffield">
                            Sheffield
                        </option>

                        <option value="Tallapoosa River Electric Coop">
                            Tallapoosa River Electric Coop
                        </option>

                        <option value="Tennessee Valley Authority (TVA)">
                            Tennessee Valley Authority (TVA)
                        </option>

                        <option value="Wiregrass Electric Coop">
                            Wiregrass Electric Coop
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-AR">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Arkansas Valley Electric Coop (AVEC)">
                            Arkansas Valley Electric Coop (AVEC)
                        </option>

                        <option value="Benton">
                            Benton
                        </option>

                        <option value="C&L Electric Coop">
                            C&L Electric Coop
                        </option>

                        <option value="Carroll Electric Coop">
                            Carroll Electric Coop
                        </option>

                        <option value="City Water & Light Plant of Jonesboro">
                            City Water & Light Plant of Jonesboro
                        </option>

                        <option value="Clarksville Light & Water Co (CL&W)">
                            Clarksville Light & Water Co (CL&W)
                        </option>

                        <option value="Clay County Electric Coop">
                            Clay County Electric Coop
                        </option>

                        <option value="Conway Corp">
                            Conway Corp
                        </option>

                        <option value="Craighead Electric Coop">
                            Craighead Electric Coop
                        </option>

                        <option value="Entergy Arkansas">
                            Entergy Arkansas
                        </option>

                        <option value="First Electric Coop">
                            First Electric Coop
                        </option>

                        <option value="Hope">
                            Hope
                        </option>

                        <option value="North Arkansas Electric Coop (NAEC)">
                            North Arkansas Electric Coop (NAEC)
                        </option>

                        <option value="North Little Rock Electric">
                            North Little Rock Electric
                        </option>

                        <option value="Oklahoma Gas & Electric (OGE)">
                            Oklahoma Gas & Electric (OGE)
                        </option>

                        <option value="Ouachita Electric Coop (OECC)">
                            Ouachita Electric Coop (OECC)
                        </option>

                        <option value="Ozarks Electric Coop">
                            Ozarks Electric Coop
                        </option>

                        <option value="Paragould">
                            Paragould
                        </option>

                        <option value="Petit Jean Electric Coop">
                            Petit Jean Electric Coop
                        </option>

                        <option value="Rich Mountain Electric Coop">
                            Rich Mountain Electric Coop
                        </option>

                        <option value=
                        "Southwest Arkansas Electric Coop (SWREA)">
                            Southwest Arkansas Electric Coop (SWREA)
                        </option>

                        <option value=
                        "Southwestern Electric Power Co (SWEPCO)">
                            Southwestern Electric Power Co (SWEPCO)
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-AZ">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Arizona Public Service (APS)">
                            Arizona Public Service (APS)
                        </option>

                        <option value="Dixie Power">
                            Dixie Power
                        </option>

                        <option value="Duncan Valley Electric Coop">
                            Duncan Valley Electric Coop
                        </option>

                        <option value="Electrical District No 2">
                            Electrical District No 2
                        </option>

                        <option value="Electrical District No 3">
                            Electrical District No 3
                        </option>

                        <option value="Electrical District No 4">
                            Electrical District No 4
                        </option>

                        <option value="Electrical District No 5">
                            Electrical District No 5
                        </option>

                        <option value="Electrical District No 6">
                            Electrical District No 6
                        </option>

                        <option value="Electrical District No 7">
                            Electrical District No 7
                        </option>

                        <option value="Electrical District No 8">
                            Electrical District No 8
                        </option>

                        <option value="Garkane Energy">
                            Garkane Energy
                        </option>

                        <option value="Graham County Electric Coop">
                            Graham County Electric Coop
                        </option>

                        <option value="Mesa">
                            Mesa
                        </option>

                        <option value="Mohave Electric Coop">
                            Mohave Electric Coop
                        </option>

                        <option value="Navajo Tribal Utility Authority (NTUA)">
                            Navajo Tribal Utility Authority (NTUA)
                        </option>

                        <option value="Navopache Electric Coop">
                            Navopache Electric Coop
                        </option>

                        <option value="Page Electric Utility">
                            Page Electric Utility
                        </option>

                        <option value="Safford">
                            Safford
                        </option>

                        <option value="Salt River Project (SRP)">
                            Salt River Project (SRP)
                        </option>

                        <option value="San Carlos Project">
                            San Carlos Project
                        </option>

                        <option value=
                        "Sulphur Springs Valley Electric Coop (SSVEC)">
                            Sulphur Springs Valley Electric Coop (SSVEC)
                        </option>

                        <option value="Thatcher">
                            Thatcher
                        </option>

                        <option value="Trico Electric Coop (TRICO)">
                            Trico Electric Coop (TRICO)
                        </option>

                        <option value="Tucson Electric Power (TEP)">
                            Tucson Electric Power (TEP)
                        </option>

                        <option value="Unisource Energy Services (UES)">
                            Unisource Energy Services (UES)
                        </option>

                        <option value="Wellton-Mohawk">
                            Wellton-Mohawk
                        </option>

                        <option value="Wickenburg">
                            Wickenburg
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-CA">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Alameda Municipal Power (Alameda MP)">
                            Alameda Municipal Power (Alameda MP)
                        </option>

                        <option value="Anaheim Public Utilities (APU)">
                            Anaheim Public Utilities (APU)
                        </option>

                        <option value="Anza Electric Coop">
                            Anza Electric Coop
                        </option>

                        <option value="Azusa Light & Water">
                            Azusa Light & Water
                        </option>

                        <option value="Banning">
                            Banning
                        </option>

                        <option value="Bear Valley">
                            Bear Valley
                        </option>

                        <option value="Burbank Water & Power (BWP)">
                            Burbank Water & Power (BWP)
                        </option>

                        <option value="Colton">
                            Colton
                        </option>

                        <option value="Glendale Water & Power (GWP)">
                            Glendale Water & Power (GWP)
                        </option>

                        <option value="Gridley Municipal Utilities (GMU)">
                            Gridley Municipal Utilities (GMU)
                        </option>

                        <option value="Healdsburg">
                            Healdsburg
                        </option>

                        <option value="Imperial Irrigation District (IID)">
                            Imperial Irrigation District (IID)
                        </option>

                        <option value="Intermountain Power Agency">
                            Intermountain Power Agency
                        </option>

                        <option value="Kirkwood Meadows PUD">
                            Kirkwood Meadows PUD
                        </option>

                        <option value="Lassen Municipal Utility District">
                            Lassen Municipal Utility District
                        </option>

                        <option value="Liberty Utilities (Algonquin)">
                            Liberty Utilities (Algonquin)
                        </option>

                        <option value="Lodi Electric Utility">
                            Lodi Electric Utility
                        </option>

                        <option value="Lompoc">
                            Lompoc
                        </option>

                        <option value="Los Angeles Dept Water & Power (LADWP)">
                            Los Angeles Dept Water & Power (LADWP)
                        </option>

                        <option value="Merced Irrigation District">
                            Merced Irrigation District
                        </option>

                        <option value="Modesto Irrigation District (MID)">
                            Modesto Irrigation District (MID)
                        </option>

                        <option value="Moreno Valley Electric Utility (MVU)">
                            Moreno Valley Electric Utility (MVU)
                        </option>

                        <option value="Needles (NPUA)">
                            Needles (NPUA)
                        </option>

                        <option value="Pacific Gas & Electric (PG&E)">
                            Pacific Gas & Electric (PG&E)
                        </option>

                        <option value="Pacific Power (PacifiCorp)">
                            Pacific Power (PacifiCorp)
                        </option>

                        <option value="Palo Alto Utilities (CPAU)">
                            Palo Alto Utilities (CPAU)
                        </option>

                        <option value="Pasadena Water & Power (PWP)">
                            Pasadena Water & Power (PWP)
                        </option>

                        <option value="Plumas-Sierra Rural Electric Coop">
                            Plumas-Sierra Rural Electric Coop
                        </option>

                        <option value="Redding Electric Utility (REU)">
                            Redding Electric Utility (REU)
                        </option>

                        <option value="Riverside Public Utilities (RPU)">
                            Riverside Public Utilities (RPU)
                        </option>

                        <option value="Roseville Electric Utility">
                            Roseville Electric Utility
                        </option>

                        <option value=
                        "Sacramento Municipal Utility District (SMUD)">
                            Sacramento Municipal Utility District (SMUD)
                        </option>

                        <option value="San Diego Gas & Electric (SDG&E)">
                            San Diego Gas & Electric (SDG&E)
                        </option>

                        <option value="San Francisco">
                            San Francisco
                        </option>

                        <option value=
                        "Santa Clara - Silicon Valley Power (SVP)">
                            Santa Clara - Silicon Valley Power (SVP)
                        </option>

                        <option value="Santa Maria Energy">
                            Santa Maria Energy
                        </option>

                        <option value="Shasta Lake">
                            Shasta Lake
                        </option>

                        <option value="Southern California Edison (SCE)">
                            Southern California Edison (SCE)
                        </option>

                        <option value="Surprise Valley Electrification Corp">
                            Surprise Valley Electrification Corp
                        </option>

                        <option value=
                        "Trinity Public Utilities District (Trinity PUD)">
                            Trinity Public Utilities District (Trinity PUD)
                        </option>

                        <option value="Truckee Donner PUD">
                            Truckee Donner PUD
                        </option>

                        <option value="Turlock Irrigation District (TID)">
                            Turlock Irrigation District (TID)
                        </option>

                        <option value="Ukiah">
                            Ukiah
                        </option>

                        <option value="Vernon">
                            Vernon
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-CO">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Black Hills Energy">
                            Black Hills Energy
                        </option>

                        <option value="Burlington">
                            Burlington
                        </option>

                        <option value="Colorado Springs Utilities (CSU)">
                            Colorado Springs Utilities (CSU)
                        </option>

                        <option value=
                        "Delta-Montrose Electric Association (DMEA)">
                            Delta-Montrose Electric Association (DMEA)
                        </option>

                        <option value="Empire Electric Association">
                            Empire Electric Association
                        </option>

                        <option value="Estes Park">
                            Estes Park
                        </option>

                        <option value="Fort Collins Utilities">
                            Fort Collins Utilities
                        </option>

                        <option value="Fort Morgan">
                            Fort Morgan
                        </option>

                        <option value="Fountain">
                            Fountain
                        </option>

                        <option value="Glenwood Springs">
                            Glenwood Springs
                        </option>

                        <option value="Grand Valley Rural">
                            Grand Valley Rural
                        </option>

                        <option value="Gunnison County Electric Association">
                            Gunnison County Electric Association
                        </option>

                        <option value="Highline Electric Association (HEA)">
                            Highline Electric Association (HEA)
                        </option>

                        <option value="Holy Cross Energy">
                            Holy Cross Energy
                        </option>

                        <option value=
                        "Intermountain Rural Electric Association Coop (IREA)">
                            Intermountain Rural Electric Association Coop
                            (IREA)
                        </option>

                        <option value="KC Electric Association">
                            KC Electric Association
                        </option>

                        <option value=
                        "KCP&L Greater Missouri Operations Co (GMO)">
                            KCP&L Greater Missouri Operations Co (GMO)
                        </option>

                        <option value="Kit Carson Electric Coop">
                            Kit Carson Electric Coop
                        </option>

                        <option value="La Plata Electric Association (LPEA)">
                            La Plata Electric Association (LPEA)
                        </option>

                        <option value="Lamar">
                            Lamar
                        </option>

                        <option value="Longmont Utilities">
                            Longmont Utilities
                        </option>

                        <option value="Loveland Water & Power (LWP)">
                            Loveland Water & Power (LWP)
                        </option>

                        <option value="Morgan County REA">
                            Morgan County REA
                        </option>

                        <option value="Mountain Parks Electric">
                            Mountain Parks Electric
                        </option>

                        <option value=
                        "Mountain View Electric Association (MVEA)">
                            Mountain View Electric Association (MVEA)
                        </option>

                        <option value="Poudre Valley REA">
                            Poudre Valley REA
                        </option>

                        <option value="San Isabel Electric Association (SIEA)">
                            San Isabel Electric Association (SIEA)
                        </option>

                        <option value="San Luis Valley REC">
                            San Luis Valley REC
                        </option>

                        <option value="San Miguel Power Association">
                            San Miguel Power Association
                        </option>

                        <option value="Sangre De Criston Electric Association">
                            Sangre De Criston Electric Association
                        </option>

                        <option value=
                        "Southeast Colorado Power Association (SECPA)">
                            Southeast Colorado Power Association (SECPA)
                        </option>

                        <option value="Springfield">
                            Springfield
                        </option>

                        <option value="Tri-County Electric Coop (TCEC)">
                            Tri-County Electric Coop (TCEC)
                        </option>

                        <option value="Trinidad">
                            Trinidad
                        </option>

                        <option value="United Power">
                            United Power
                        </option>

                        <option value="White River Electric Association">
                            White River Electric Association
                        </option>

                        <option value="Xcel Energy">
                            Xcel Energy
                        </option>

                        <option value=
                        "Yampa Valley Electric Association (YVEA)">
                            Yampa Valley Electric Association (YVEA)
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-CT">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Abest Power and Gas">
                            Abest Power and Gas
                        </option>

                        <option value="Ambit Energy">
                            Ambit Energy
                        </option>

                        <option value="Bozrah Light & Power">
                            Bozrah Light & Power
                        </option>

                        <option value=
                        "Connecticut Light & Power (CL&P, Northeast Utilities)">
                        Connecticut Light & Power (CL&P, Northeast Utilities)
                        </option>

                        <option value="Direct Energy">
                            Direct Energy
                        </option>

                        <option value="Groton">
                            Groton
                        </option>

                        <option value="Jewett City">
                            Jewett City
                        </option>

                        <option value="North American Power">
                            North American Power
                        </option>

                        <option value="Norwich Public Utilities (NPU)">
                            Norwich Public Utilities (NPU)
                        </option>

                        <option value="Starion Energy">
                            Starion Energy
                        </option>

                        <option value="United Illuminating (UI)">
                            United Illuminating (UI)
                        </option>

                        <option value="Verde Energy">
                            Verde Energy
                        </option>

                        <option value="Viridian Energy">
                            Viridian Energy
                        </option>

                        <option value="Wallingford Public Utilities">
                            Wallingford Public Utilities
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-DC">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Direct Energy">
                            Direct Energy
                        </option>

                        <option value="Potomac Electric Power Co (PEPCO)">
                            Potomac Electric Power Co (PEPCO)
                        </option>

                        <option value="Starion Energy">
                            Starion Energy
                        </option>

                        <option value="Washington Gas Energy Services (WGES)">
                            Washington Gas Energy Services (WGES)
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-DE">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Delaware Electric Coop">
                            Delaware Electric Coop
                        </option>

                        <option value="Delmarva Power & Light (DPL)">
                            Delmarva Power & Light (DPL)
                        </option>

                        <option value="Dover Public Utilities Department">
                            Dover Public Utilities Department
                        </option>

                        <option value="Just Energy">
                            Just Energy
                        </option>

                        <option value="Middletown">
                            Middletown
                        </option>

                        <option value="Milford">
                            Milford
                        </option>

                        <option value="Newark">
                            Newark
                        </option>

                        <option value="Smyrna">
                            Smyrna
                        </option>

                        <option value="Starion Energy">
                            Starion Energy
                        </option>

                        <option value="Washington Gas Energy Services (WGES)">
                            Washington Gas Energy Services (WGES)
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-FL">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Bartow City Electric Department">
                            Bartow City Electric Department
                        </option>

                        <option value="Beaches Energy Services">
                            Beaches Energy Services
                        </option>

                        <option value="Central Florida Electric Coop (CFEC)">
                            Central Florida Electric Coop (CFEC)
                        </option>

                        <option value="Choctawhatchee Electric Coop (CHELCO)">
                            Choctawhatchee Electric Coop (CHELCO)
                        </option>

                        <option value="Clay Electric Coop">
                            Clay Electric Coop
                        </option>

                        <option value="Direct Energy">
                            Direct Energy
                        </option>

                        <option value="Duke Energy">
                            Duke Energy
                        </option>

                        <option value="Florida Keys Electric Coop (FKEC)">
                            Florida Keys Electric Coop (FKEC)
                        </option>

                        <option value="Florida Power & Light (FPL)">
                            Florida Power & Light (FPL)
                        </option>

                        <option value="Florida Public Utilities (FPU)">
                            Florida Public Utilities (FPU)
                        </option>

                        <option value="Fort Meade">
                            Fort Meade
                        </option>

                        <option value="Fort Pierce Utilities Authority">
                            Fort Pierce Utilities Authority
                        </option>

                        <option value="Gainesville Regional Utilities (GRU)">
                            Gainesville Regional Utilities (GRU)
                        </option>

                        <option value="Glades Electric Coop">
                            Glades Electric Coop
                        </option>

                        <option value="Gulf Coast Electric Coop">
                            Gulf Coast Electric Coop
                        </option>

                        <option value="Gulf Power">
                            Gulf Power
                        </option>

                        <option value="Homestead Public Services (HPS)">
                            Homestead Public Services (HPS)
                        </option>

                        <option value="JEA">
                            JEA
                        </option>

                        <option value="Keys Energy Services">
                            Keys Energy Services
                        </option>

                        <option value="Kissimmee Utility Authority (KUA)">
                            Kissimmee Utility Authority (KUA)
                        </option>

                        <option value="Lake Worth">
                            Lake Worth
                        </option>

                        <option value="Lakeland Electric">
                            Lakeland Electric
                        </option>

                        <option value="Lee County Electric Coop (LCEC)">
                            Lee County Electric Coop (LCEC)
                        </option>

                        <option value="Leesburg">
                            Leesburg
                        </option>

                        <option value="Mount Dora">
                            Mount Dora
                        </option>

                        <option value=
                        "New Smyrna Beach Utilities Commission (UCNSB)">
                            New Smyrna Beach Utilities Commission (UCNSB)
                        </option>

                        <option value="Ocala Utility Services">
                            Ocala Utility Services
                        </option>

                        <option value="Okefenoke REMC (OREMC)">
                            Okefenoke REMC (OREMC)
                        </option>

                        <option value="Orlando Utilities Commission (OUC)">
                            Orlando Utilities Commission (OUC)
                        </option>

                        <option value="Peace River Electric Coop (PRECO)">
                            Peace River Electric Coop (PRECO)
                        </option>

                        <option value="Progress Energy Florida">
                            Progress Energy Florida
                        </option>

                        <option value="Quincy">
                            Quincy
                        </option>

                        <option value="Sumter Electric Coop (SECO)">
                            Sumter Electric Coop (SECO)
                        </option>

                        <option value="Suwannee Valley Electric Coop">
                            Suwannee Valley Electric Coop
                        </option>

                        <option value="Tallahassee City Electric">
                            Tallahassee City Electric
                        </option>

                        <option value="Talquin Electric Coop">
                            Talquin Electric Coop
                        </option>

                        <option value="Tampa Electric Co (TECO)">
                            Tampa Electric Co (TECO)
                        </option>

                        <option value="Tri-County Electric Coop (TCEC Texas)">
                            Tri-County Electric Coop (TCEC Texas)
                        </option>

                        <option value="Vero Beach">
                            Vero Beach
                        </option>

                        <option value="West Florida Electric Coop (WFEC)">
                            West Florida Electric Coop (WFEC)
                        </option>

                        <option value=
                        "Withlacoochee River Electric Coop (WREC)">
                            Withlacoochee River Electric Coop (WREC)
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-GA">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Acworth">
                            Acworth
                        </option>

                        <option value="Albany Water, Gas & Light">
                            Albany Water, Gas & Light
                        </option>

                        <option value="Amicalola EMC">
                            Amicalola EMC
                        </option>

                        <option value="Blue Ridge Mountain EMC (BRMEMC)">
                            Blue Ridge Mountain EMC (BRMEMC)
                        </option>

                        <option value="Buford">
                            Buford
                        </option>

                        <option value="Canoochee EMC">
                            Canoochee EMC
                        </option>

                        <option value="Carroll EMC (CEMC)">
                            Carroll EMC (CEMC)
                        </option>

                        <option value="Cartersville">
                            Cartersville
                        </option>

                        <option value="Central Georgia EMC (CGEMC)">
                            Central Georgia EMC (CGEMC)
                        </option>

                        <option value="Coastal EMC">
                            Coastal EMC
                        </option>

                        <option value="Cobb EMC">
                            Cobb EMC
                        </option>

                        <option value="Colquitt EMC">
                            Colquitt EMC
                        </option>

                        <option value="Constellation Energy">
                            Constellation Energy
                        </option>

                        <option value="Covington">
                            Covington
                        </option>

                        <option value="Coweta-Fayette EMC">
                            Coweta-Fayette EMC
                        </option>

                        <option value="Diverse Power">
                            Diverse Power
                        </option>

                        <option value="Douglas">
                            Douglas
                        </option>

                        <option value="Excelsior EMC">
                            Excelsior EMC
                        </option>

                        <option value="Flint Energies">
                            Flint Energies
                        </option>

                        <option value="Georgia Power">
                            Georgia Power
                        </option>

                        <option value="GreyStone Power">
                            GreyStone Power
                        </option>

                        <option value="Griffin">
                            Griffin
                        </option>

                        <option value="Habersham EMC">
                            Habersham EMC
                        </option>

                        <option value="Hart EMC">
                            Hart EMC
                        </option>

                        <option value="Jackson EMC">
                            Jackson EMC
                        </option>

                        <option value="Jackson Electric Department">
                            Jackson Electric Department
                        </option>

                        <option value="Jefferson Energy Coop (JEC)">
                            Jefferson Energy Coop (JEC)
                        </option>

                        <option value="Lawrenceville">
                            Lawrenceville
                        </option>

                        <option value="Marietta Power & Water">
                            Marietta Power & Water
                        </option>

                        <option value="Mitchell EMC">
                            Mitchell EMC
                        </option>

                        <option value="Monroe">
                            Monroe
                        </option>

                        <option value="Norcross">
                            Norcross
                        </option>

                        <option value="North American Power">
                            North American Power
                        </option>

                        <option value="North Georgia EMC">
                            North Georgia EMC
                        </option>

                        <option value="Oconee EMC">
                            Oconee EMC
                        </option>

                        <option value="Okefenoke REMC (OREMC)">
                            Okefenoke REMC (OREMC)
                        </option>

                        <option value="Palmetto Utilities">
                            Palmetto Utilities
                        </option>

                        <option value="Planters EMC">
                            Planters EMC
                        </option>

                        <option value="Satilla REMC">
                            Satilla REMC
                        </option>

                        <option value="Sawnee EMC">
                            Sawnee EMC
                        </option>

                        <option value="Snapping Shoals EMC (SSEMC)">
                            Snapping Shoals EMC (SSEMC)
                        </option>

                        <option value="Southern Rivers Energy">
                            Southern Rivers Energy
                        </option>

                        <option value="Sumter EMC">
                            Sumter EMC
                        </option>

                        <option value="Thomaston">
                            Thomaston
                        </option>

                        <option value="Thomasville">
                            Thomasville
                        </option>

                        <option value="Three Notch EMC">
                            Three Notch EMC
                        </option>

                        <option value="Tri-County EMC">
                            Tri-County EMC
                        </option>

                        <option value="Tri-State EMC">
                            Tri-State EMC
                        </option>

                        <option value="Union City">
                            Union City
                        </option>

                        <option value="Upton EMC">
                            Upton EMC
                        </option>

                        <option value="Walton EMC">
                            Walton EMC
                        </option>

                        <option value="Washington">
                            Washington
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-HI">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Hawaii Electric Light Co (HELCO)">
                            Hawaii Electric Light Co (HELCO)
                        </option>

                        <option value="Hawaiian Electric Co (HECO)">
                            Hawaiian Electric Co (HECO)
                        </option>

                        <option value="Kauai Island Utility Coop (KIUC)">
                            Kauai Island Utility Coop (KIUC)
                        </option>

                        <option value="Maui Electric Co (MECO)">
                            Maui Electric Co (MECO)
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-IA">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Access Energy Coop">
                            Access Energy Coop
                        </option>

                        <option value="Alliant Energy">
                            Alliant Energy
                        </option>

                        <option value="Ames Electric Department">
                            Ames Electric Department
                        </option>

                        <option value="Butler County REC">
                            Butler County REC
                        </option>

                        <option value="Danville">
                            Danville
                        </option>

                        <option value="Eastern Iowa REC">
                            Eastern Iowa REC
                        </option>

                        <option value="Harrison County REC (HCREC)">
                            Harrison County REC (HCREC)
                        </option>

                        <option value="Independence">
                            Independence
                        </option>

                        <option value="Interstate Power & Light (IPL)">
                            Interstate Power & Light (IPL)
                        </option>

                        <option value="L&O Power Coop">
                            L&O Power Coop
                        </option>

                        <option value="Linn County REC">
                            Linn County REC
                        </option>

                        <option value="MidAmerican Energy">
                            MidAmerican Energy
                        </option>

                        <option value="Midland Power Coop">
                            Midland Power Coop
                        </option>

                        <option value="Muscatine Power and Water">
                            Muscatine Power and Water
                        </option>

                        <option value="Prairie Energy Coop">
                            Prairie Energy Coop
                        </option>

                        <option value="Southern Iowa Electric Coop">
                            Southern Iowa Electric Coop
                        </option>

                        <option value="TIP Rural Electric Coop">
                            TIP Rural Electric Coop
                        </option>

                        <option value="Western Iowa Power Coop (WIPCO)">
                            Western Iowa Power Coop (WIPCO)
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-ID">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Avista">
                            Avista
                        </option>

                        <option value="Clearwater Power Co">
                            Clearwater Power Co
                        </option>

                        <option value="Idaho Falls Power">
                            Idaho Falls Power
                        </option>

                        <option value="Idaho Power">
                            Idaho Power
                        </option>

                        <option value="Kootenai Electric Coop (KEC)">
                            Kootenai Electric Coop (KEC)
                        </option>

                        <option value="Lower Valley Energy">
                            Lower Valley Energy
                        </option>

                        <option value="Northern Lights Electric Coop (NLI)">
                            Northern Lights Electric Coop (NLI)
                        </option>

                        <option value="Rocky Mountain Power (PacifiCorp)">
                            Rocky Mountain Power (PacifiCorp)
                        </option>

                        <option value="Rupert">
                            Rupert
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-IL">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Ameren Illinois">
                            Ameren Illinois
                        </option>

                        <option value="Batavia">
                            Batavia
                        </option>

                        <option value="Cairo">
                            Cairo
                        </option>

                        <option value="Casey">
                            Casey
                        </option>

                        <option value="Champion Energy">
                            Champion Energy
                        </option>

                        <option value="Clay Electric Coop (CECI)">
                            Clay Electric Coop (CECI)
                        </option>

                        <option value="Clinton County Electric Coop (CCECI)">
                            Clinton County Electric Coop (CCECI)
                        </option>

                        <option value="Coles-Moultrie Electric Coop">
                            Coles-Moultrie Electric Coop
                        </option>

                        <option value="Commonwealth Edison (ComEd)">
                            Commonwealth Edison (ComEd)
                        </option>

                        <option value="Corn Belt Energy">
                            Corn Belt Energy
                        </option>

                        <option value="Direct Energy">
                            Direct Energy
                        </option>

                        <option value="Eastern Illinois Electric Coop">
                            Eastern Illinois Electric Coop
                        </option>

                        <option value="Egyptian Electric Coop">
                            Egyptian Electric Coop
                        </option>

                        <option value="EnerStar Power Corp">
                            EnerStar Power Corp
                        </option>

                        <option value="Fairfield">
                            Fairfield
                        </option>

                        <option value="Flora">
                            Flora
                        </option>

                        <option value="Green Mountain Energy">
                            Green Mountain Energy
                        </option>

                        <option value="IGS Energy">
                            IGS Energy
                        </option>

                        <option value="Illinois REC">
                            Illinois REC
                        </option>

                        <option value="Jo-Carroll Energy Coop">
                            Jo-Carroll Energy Coop
                        </option>

                        <option value="MJM Electric Coop">
                            MJM Electric Coop
                        </option>

                        <option value="Menard Electric Coop">
                            Menard Electric Coop
                        </option>

                        <option value="MidAmerican Energy">
                            MidAmerican Energy
                        </option>

                        <option value="Naperville Public Utilities">
                            Naperville Public Utilities
                        </option>

                        <option value="North American Power">
                            North American Power
                        </option>

                        <option value="Rantoul">
                            Rantoul
                        </option>

                        <option value="Rock Energy Cooperative">
                            Rock Energy Cooperative
                        </option>

                        <option value="Shelby Electric Coop">
                            Shelby Electric Coop
                        </option>

                        <option value=
                        "Southeastern Illinois Electric Coop (SEIEC)">
                            Southeastern Illinois Electric Coop (SEIEC)
                        </option>

                        <option value="Southern Illinois Electric Coop (SIEC)">
                            Southern Illinois Electric Coop (SIEC)
                        </option>

                        <option value="Southwestern Electric Coop (SWECI)">
                            Southwestern Electric Coop (SWECI)
                        </option>

                        <option value="Springfield">
                            Springfield
                        </option>

                        <option value="Starion Energy">
                            Starion Energy
                        </option>

                        <option value="Tri-County Electric Coop">
                            Tri-County Electric Coop
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-IN">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Anderson Municipal Light & Power">
                            Anderson Municipal Light & Power
                        </option>

                        <option value="City of Columbia City Utilities">
                            City of Columbia City Utilities
                        </option>

                        <option value="Clark County REMC">
                            Clark County REMC
                        </option>

                        <option value="Direct Energy">
                            Direct Energy
                        </option>

                        <option value="Duke Energy">
                            Duke Energy
                        </option>

                        <option value="Fulton County REMC">
                            Fulton County REMC
                        </option>

                        <option value="Garrett">
                            Garrett
                        </option>

                        <option value="Harrison County REMC">
                            Harrison County REMC
                        </option>

                        <option value="Hendricks Power Coop">
                            Hendricks Power Coop
                        </option>

                        <option value="Indiana Michigan Power (AEP)">
                            Indiana Michigan Power (AEP)
                        </option>

                        <option value="Indianapolis Power & Light Co (IPL)">
                            Indianapolis Power & Light Co (IPL)
                        </option>

                        <option value="Jackson County REMC">
                            Jackson County REMC
                        </option>

                        <option value="Johnson County REMC">
                            Johnson County REMC
                        </option>

                        <option value="Kankakee Valley REMC (KVREMC)">
                            Kankakee Valley REMC (KVREMC)
                        </option>

                        <option value="Lagrange County REMC">
                            Lagrange County REMC
                        </option>

                        <option value="Mishawaka Utilities">
                            Mishawaka Utilities
                        </option>

                        <option value="Ninestar">
                            Ninestar
                        </option>

                        <option value="Northeastern REMC">
                            Northeastern REMC
                        </option>

                        <option value="Northeastern Utilities (NU)">
                            Northeastern Utilities (NU)
                        </option>

                        <option value=
                        "Northern Indiana Public Service Co (NIPSCO)">
                            Northern Indiana Public Service Co (NIPSCO)
                        </option>

                        <option value="Paoli">
                            Paoli
                        </option>

                        <option value="Parke County REMC">
                            Parke County REMC
                        </option>

                        <option value="RushShelby Energy">
                            RushShelby Energy
                        </option>

                        <option value="South Central Indiana REMC (SCIREMC)">
                            South Central Indiana REMC (SCIREMC)
                        </option>

                        <option value="Southeastern Indiana REMC (SEIREMC)">
                            Southeastern Indiana REMC (SEIREMC)
                        </option>

                        <option value="Southern Indiana REC">
                            Southern Indiana REC
                        </option>

                        <option value="Tipmont REMC">
                            Tipmont REMC
                        </option>

                        <option value="Tipton Municipal Utilities">
                            Tipton Municipal Utilities
                        </option>

                        <option value="Vectren Corp">
                            Vectren Corp
                        </option>

                        <option value="Wabash County REMC">
                            Wabash County REMC
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-KS">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Black Hills Energy">
                            Black Hills Energy
                        </option>

                        <option value="Caney Valley Electric Coop">
                            Caney Valley Electric Coop
                        </option>

                        <option value="DS&O Rural Electric Coop">
                            DS&O Rural Electric Coop
                        </option>

                        <option value="Flint Hills RECA">
                            Flint Hills RECA
                        </option>

                        <option value="Gardner">
                            Gardner
                        </option>

                        <option value="Heartland REC">
                            Heartland REC
                        </option>

                        <option value=
                        "Kansas City Board of Public Utilities (BPU)">
                            Kansas City Board of Public Utilities (BPU)
                        </option>

                        <option value="Kansas City Power & Light (KCP&L)">
                            Kansas City Power & Light (KCP&L)
                        </option>

                        <option value="Kansas Gas Service">
                            Kansas Gas Service
                        </option>

                        <option value="Kaw Valley Electric Coop">
                            Kaw Valley Electric Coop
                        </option>

                        <option value="La Harpe">
                            La Harpe
                        </option>

                        <option value="Leavenworth-Jefferson Electric Coop">
                            Leavenworth-Jefferson Electric Coop
                        </option>

                        <option value="Midwest Energy">
                            Midwest Energy
                        </option>

                        <option value="Pioneer Electric Coop">
                            Pioneer Electric Coop
                        </option>

                        <option value="Prairie Land Electric">
                            Prairie Land Electric
                        </option>

                        <option value="St Francis">
                            St Francis
                        </option>

                        <option value="Tri-County Electric Coop (TCEC)">
                            Tri-County Electric Coop (TCEC)
                        </option>

                        <option value="Wamego">
                            Wamego
                        </option>

                        <option value="Westar Energy">
                            Westar Energy
                        </option>

                        <option value="Western Coop Electric Association">
                            Western Coop Electric Association
                        </option>

                        <option value="Wheatland Electric Coop (WECI)">
                            Wheatland Electric Coop (WECI)
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-KY">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Benton">
                            Benton
                        </option>

                        <option value="Berea Municipal Utility">
                            Berea Municipal Utility
                        </option>

                        <option value="Big Rivers Electric Coop">
                            Big Rivers Electric Coop
                        </option>

                        <option value="Blue Grass Energy">
                            Blue Grass Energy
                        </option>

                        <option value="Clark Energy Coop">
                            Clark Energy Coop
                        </option>

                        <option value="Direct Energy">
                            Direct Energy
                        </option>

                        <option value="Duke Energy">
                            Duke Energy
                        </option>

                        <option value="Fleming-Mason Energy Coop">
                            Fleming-Mason Energy Coop
                        </option>

                        <option value="Franklin">
                            Franklin
                        </option>

                        <option value="Glasgow">
                            Glasgow
                        </option>

                        <option value="Grayson RECC">
                            Grayson RECC
                        </option>

                        <option value="Henderson">
                            Henderson
                        </option>

                        <option value="Hickman">
                            Hickman
                        </option>

                        <option value="Inter Country Energy Coop">
                            Inter Country Energy Coop
                        </option>

                        <option value="Jackson Energy Coop">
                            Jackson Energy Coop
                        </option>

                        <option value=
                        "Jackson Purchase Energy Corp (JP Energy)">
                            Jackson Purchase Energy Corp (JP Energy)
                        </option>

                        <option value="Kenergy Corp">
                            Kenergy Corp
                        </option>

                        <option value="Kentucky Power">
                            Kentucky Power
                        </option>

                        <option value="Kentucky Utilities (KU)">
                            Kentucky Utilities (KU)
                        </option>

                        <option value="Louisville Gas & Electric (LG&E)">
                            Louisville Gas & Electric (LG&E)
                        </option>

                        <option value="Nolin RECC">
                            Nolin RECC
                        </option>

                        <option value="Owen Electric Coop">
                            Owen Electric Coop
                        </option>

                        <option value="Owensboro Municipal Utilities (OMU)">
                            Owensboro Municipal Utilities (OMU)
                        </option>

                        <option value="Paducah">
                            Paducah
                        </option>

                        <option value="Pennyrile REC (PRECC)">
                            Pennyrile REC (PRECC)
                        </option>

                        <option value="Salt River Electric">
                            Salt River Electric
                        </option>

                        <option value="Shelby Energy Coop">
                            Shelby Energy Coop
                        </option>

                        <option value="South Kentucky RECC (SKRECC)">
                            South Kentucky RECC (SKRECC)
                        </option>

                        <option value="Warren RECC (WRECC)">
                            Warren RECC (WRECC)
                        </option>

                        <option value="West Kentucky RECC (WKRECC)">
                            West Kentucky RECC (WKRECC)
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-LA">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Alexandria">
                            Alexandria
                        </option>

                        <option value="Beauregard Electric Coop (BECI)">
                            Beauregard Electric Coop (BECI)
                        </option>

                        <option value="Central Louisiana Electric Co (CLECO)">
                            Central Louisiana Electric Co (CLECO)
                        </option>

                        <option value="Claiborne Electric Coop">
                            Claiborne Electric Coop
                        </option>

                        <option value="Concordia Electric Coop">
                            Concordia Electric Coop
                        </option>

                        <option value="Dixie EMC (DEMCO)">
                            Dixie EMC (DEMCO)
                        </option>

                        <option value="Entergy Gulf States Louisiana">
                            Entergy Gulf States Louisiana
                        </option>

                        <option value="Entergy Louisiana">
                            Entergy Louisiana
                        </option>

                        <option value="Entergy New Orleans">
                            Entergy New Orleans
                        </option>

                        <option value="Jefferson Davis Electric Coop">
                            Jefferson Davis Electric Coop
                        </option>

                        <option value="Lafayette Utilities System (LUS)">
                            Lafayette Utilities System (LUS)
                        </option>

                        <option value="Minden">
                            Minden
                        </option>

                        <option value="Natchitoches">
                            Natchitoches
                        </option>

                        <option value="Northeast Louisiana Power Coop">
                            Northeast Louisiana Power Coop
                        </option>

                        <option value="Plaquemine">
                            Plaquemine
                        </option>

                        <option value="Pointe Coupee EMC">
                            Pointe Coupee EMC
                        </option>

                        <option value="South Louisiana Electric Coop (SLECA)">
                            South Louisiana Electric Coop (SLECA)
                        </option>

                        <option value=
                        "Southwest Louisiana Electric Membership Co (SLEMCO)">
                            Southwest Louisiana Electric Membership Co (SLEMCO)
                        </option>

                        <option value=
                        "Southwestern Electric Power Co (SWEPCO)">
                            Southwestern Electric Power Co (SWEPCO)
                        </option>

                        <option value=
                        "Washington-St. Tammany Electric Coop (WSTE)">
                            Washington-St. Tammany Electric Coop (WSTE)
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-MA">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Ashburnham">
                            Ashburnham
                        </option>

                        <option value="Belmont">
                            Belmont
                        </option>

                        <option value=
                        "Braintree Electric Light Department (BELD)">
                            Braintree Electric Light Department (BELD)
                        </option>

                        <option value="Chester">
                            Chester
                        </option>

                        <option value=
                        "Chicopee Electric Light Department (CELD)">
                            Chicopee Electric Light Department (CELD)
                        </option>

                        <option value="Concord">
                            Concord
                        </option>

                        <option value="Danvers">
                            Danvers
                        </option>

                        <option value="Direct Energy">
                            Direct Energy
                        </option>

                        <option value=
                        "Fitchburg Gas & Electric Light (Unitil)">
                            Fitchburg Gas & Electric Light (Unitil)
                        </option>

                        <option value="Groton">
                            Groton
                        </option>

                        <option value="Groveland">
                            Groveland
                        </option>

                        <option value="Hingham">
                            Hingham
                        </option>

                        <option value="Holden Municipal Light Department">
                            Holden Municipal Light Department
                        </option>

                        <option value="Holyoke Gas & Electric (HG&E)">
                            Holyoke Gas & Electric (HG&E)
                        </option>

                        <option value="Hudson Light and Power">
                            Hudson Light and Power
                        </option>

                        <option value="Hull Municipal Light Plant">
                            Hull Municipal Light Plant
                        </option>

                        <option value="Ipswich">
                            Ipswich
                        </option>

                        <option value="Just Energy">
                            Just Energy
                        </option>

                        <option value=
                        "Littleton Electric Light Department (LELD)">
                            Littleton Electric Light Department (LELD)
                        </option>

                        <option value=
                        "Mansfield Municipal Electric Department (MMED)">
                            Mansfield Municipal Electric Department (MMED)
                        </option>

                        <option value="Marblehead">
                            Marblehead
                        </option>

                        <option value="Merrimac">
                            Merrimac
                        </option>

                        <option value="Middleborough Gas & Electric (MGED)">
                            Middleborough Gas & Electric (MGED)
                        </option>

                        <option value="NSTAR">
                            NSTAR
                        </option>

                        <option value="Nantucket Electric Co">
                            Nantucket Electric Co
                        </option>

                        <option value="National Grid">
                            National Grid
                        </option>

                        <option value="North Attleborough">
                            North Attleborough
                        </option>

                        <option value="Norwood">
                            Norwood
                        </option>

                        <option value="Paxton">
                            Paxton
                        </option>

                        <option value="Peabody Municipal Light Plant (PLMP)">
                            Peabody Municipal Light Plant (PLMP)
                        </option>

                        <option value=
                        "Reading Municipal Light Department (RMLD)">
                            Reading Municipal Light Department (RMLD)
                        </option>

                        <option value="Rowley">
                            Rowley
                        </option>

                        <option value="Shrewsbury Electric (SELCO)">
                            Shrewsbury Electric (SELCO)
                        </option>

                        <option value=
                        "South Hadley Electric Light Department (SHELD)">
                            South Hadley Electric Light Department (SHELD)
                        </option>

                        <option value="Starion Energy">
                            Starion Energy
                        </option>

                        <option value="Sterling">
                            Sterling
                        </option>

                        <option value=
                        "Taunton Municipal Lighting Plant (TMLP)">
                            Taunton Municipal Lighting Plant (TMLP)
                        </option>

                        <option value="Templeton Light & Water">
                            Templeton Light & Water
                        </option>

                        <option value="Unitil Energy Systems">
                            Unitil Energy Systems
                        </option>

                        <option value="Verde Energy">
                            Verde Energy
                        </option>

                        <option value="Viridian Energy">
                            Viridian Energy
                        </option>

                        <option value="Wakefield">
                            Wakefield
                        </option>

                        <option value="Wellesley">
                            Wellesley
                        </option>

                        <option value="West Boylston (WBMLP)">
                            West Boylston (WBMLP)
                        </option>

                        <option value=
                        "Western Massachusetts Electric Co (WMECO)">
                            Western Massachusetts Electric Co (WMECO)
                        </option>

                        <option value="Westfield Gas + Electric (WG+E)">
                            Westfield Gas + Electric (WG+E)
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-MD">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Baltimore Gas & Electric (BGE)">
                            Baltimore Gas & Electric (BGE)
                        </option>

                        <option value="Choptank Electric Coop">
                            Choptank Electric Coop
                        </option>

                        <option value="Constellation Energy">
                            Constellation Energy
                        </option>

                        <option value="Delmarva Power & Light (DPL)">
                            Delmarva Power & Light (DPL)
                        </option>

                        <option value="Easton Utilities Commission">
                            Easton Utilities Commission
                        </option>

                        <option value="Green Mountain Energy">
                            Green Mountain Energy
                        </option>

                        <option value="Hagerstown">
                            Hagerstown
                        </option>

                        <option value="North American Power">
                            North American Power
                        </option>

                        <option value="Potomac Edison (FirstEnergy)">
                            Potomac Edison (FirstEnergy)
                        </option>

                        <option value="Potomac Electric Power Co (PEPCO)">
                            Potomac Electric Power Co (PEPCO)
                        </option>

                        <option value="Somerset REC">
                            Somerset REC
                        </option>

                        <option value=
                        "Southern Maryland Electric Coop (SMECO)">
                            Southern Maryland Electric Coop (SMECO)
                        </option>

                        <option value="Starion Energy">
                            Starion Energy
                        </option>

                        <option value="Thurmont Municipal Light Co">
                            Thurmont Municipal Light Co
                        </option>

                        <option value="Washington Gas Energy Services (WGES)">
                            Washington Gas Energy Services (WGES)
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-ME">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Bangor Hydro (Emera)">
                            Bangor Hydro (Emera)
                        </option>

                        <option value="Central Maine Power Co (CMPCO)">
                            Central Maine Power Co (CMPCO)
                        </option>

                        <option value="Electricity Maine">
                            Electricity Maine
                        </option>

                        <option value="Unitil Energy Systems">
                            Unitil Energy Systems
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-MI">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Alpena Power Co">
                            Alpena Power Co
                        </option>

                        <option value="Bay City Electric Department">
                            Bay City Electric Department
                        </option>

                        <option value="Cherryland Electric Coop (CECELEC)">
                            Cherryland Electric Coop (CECELEC)
                        </option>

                        <option value="Cloverland Electric Coop">
                            Cloverland Electric Coop
                        </option>

                        <option value="Consumers Energy">
                            Consumers Energy
                        </option>

                        <option value="Croswell">
                            Croswell
                        </option>

                        <option value="DTE Energy (Detroit Edison)">
                            DTE Energy (Detroit Edison)
                        </option>

                        <option value="Direct Energy">
                            Direct Energy
                        </option>

                        <option value="Great Lakes Energy Coop">
                            Great Lakes Energy Coop
                        </option>

                        <option value="Holland Board of Public Works (HBPW)">
                            Holland Board of Public Works (HBPW)
                        </option>

                        <option value="HomeWorks Tri-County Electric Coop">
                            HomeWorks Tri-County Electric Coop
                        </option>

                        <option value="Indiana Michigan Power (AEP)">
                            Indiana Michigan Power (AEP)
                        </option>

                        <option value="Lansing Board of Water & Light (LBWL)">
                            Lansing Board of Water & Light (LBWL)
                        </option>

                        <option value="Midwest Energy Coop">
                            Midwest Energy Coop
                        </option>

                        <option value="Niles">
                            Niles
                        </option>

                        <option value="Petoskey">
                            Petoskey
                        </option>

                        <option value=
                        "Presque Isle Electric & Gas Coop (PIEG)">
                            Presque Isle Electric & Gas Coop (PIEG)
                        </option>

                        <option value="Sturgis">
                            Sturgis
                        </option>

                        <option value="Union City">
                            Union City
                        </option>

                        <option value="Upper Peninsula Power Co (UPPCO)">
                            Upper Peninsula Power Co (UPPCO)
                        </option>

                        <option value="WE Energies">
                            WE Energies
                        </option>

                        <option value="Xcel Energy">
                            Xcel Energy
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-MN">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Alexandria">
                            Alexandria
                        </option>

                        <option value="Alliant Energy">
                            Alliant Energy
                        </option>

                        <option value="Anoka">
                            Anoka
                        </option>

                        <option value="Beltrami Electric Coop">
                            Beltrami Electric Coop
                        </option>

                        <option value="Brown County REA">
                            Brown County REA
                        </option>

                        <option value="Connexus Energy">
                            Connexus Energy
                        </option>

                        <option value="Crow Wing Power (CW Power)">
                            Crow Wing Power (CW Power)
                        </option>

                        <option value="Dakota Electric">
                            Dakota Electric
                        </option>

                        <option value="Direct Energy">
                            Direct Energy
                        </option>

                        <option value="East Central Energy (ECE)">
                            East Central Energy (ECE)
                        </option>

                        <option value="Great River Energy">
                            Great River Energy
                        </option>

                        <option value="Hutchinson Utilities Comm">
                            Hutchinson Utilities Comm
                        </option>

                        <option value="Interstate Power & Light (IPL)">
                            Interstate Power & Light (IPL)
                        </option>

                        <option value="L&O Power Coop">
                            L&O Power Coop
                        </option>

                        <option value="Lake Country Power Coop">
                            Lake Country Power Coop
                        </option>

                        <option value="Lake Region Electric Coop (LREC)">
                            Lake Region Electric Coop (LREC)
                        </option>

                        <option value="McLeod Coop">
                            McLeod Coop
                        </option>

                        <option value="Meeker Coop">
                            Meeker Coop
                        </option>

                        <option value="Mille Lacs Electric Coop (MLEC)">
                            Mille Lacs Electric Coop (MLEC)
                        </option>

                        <option value="Minnesota Power (MN Power)">
                            Minnesota Power (MN Power)
                        </option>

                        <option value="Minnesota Valley Electric Coop (MVEC)">
                            Minnesota Valley Electric Coop (MVEC)
                        </option>

                        <option value="Minnesota Valley REC">
                            Minnesota Valley REC
                        </option>

                        <option value="Minnkota Power Coop">
                            Minnkota Power Coop
                        </option>

                        <option value="Otter Tail Power Co">
                            Otter Tail Power Co
                        </option>

                        <option value="People's Coop">
                            People's Coop
                        </option>

                        <option value="Rochester Public Utilities (RPU)">
                            Rochester Public Utilities (RPU)
                        </option>

                        <option value="Runestone Electric Association">
                            Runestone Electric Association
                        </option>

                        <option value=
                        "South Central Electric Association (SCEA)">
                            South Central Electric Association (SCEA)
                        </option>

                        <option value="Stearns Electric Association">
                            Stearns Electric Association
                        </option>

                        <option value=
                        "Steele-Waseca Cooperative Electric (SWCE)">
                            Steele-Waseca Cooperative Electric (SWCE)
                        </option>

                        <option value="Todd-Wadena Electric Coop">
                            Todd-Wadena Electric Coop
                        </option>

                        <option value="Tri-County Electric (TCE)">
                            Tri-County Electric (TCE)
                        </option>

                        <option value=
                        "Wright-Hennepin Coop Electric Association (WHE)">
                            Wright-Hennepin Coop Electric Association (WHE)
                        </option>

                        <option value="Xcel Energy">
                            Xcel Energy
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-MO">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Black River Electric Coop">
                            Black River Electric Coop
                        </option>

                        <option value="Boone Electric Coop">
                            Boone Electric Coop
                        </option>

                        <option value="Carroll Electric Coop">
                            Carroll Electric Coop
                        </option>

                        <option value="Citizens Electric Corp (CECMO)">
                            Citizens Electric Corp (CECMO)
                        </option>

                        <option value="City Utilities of Springfield">
                            City Utilities of Springfield
                        </option>

                        <option value="City of Columbia Utilities">
                            City of Columbia Utilities
                        </option>

                        <option value="Co-Mo Electric Coop (Co-Mo)">
                            Co-Mo Electric Coop (Co-Mo)
                        </option>

                        <option value="Cuivre River Electric Coop">
                            Cuivre River Electric Coop
                        </option>

                        <option value="Empire District Electric Co (EDE)">
                            Empire District Electric Co (EDE)
                        </option>

                        <option value="Farmington">
                            Farmington
                        </option>

                        <option value="Grundy Electric Coop">
                            Grundy Electric Coop
                        </option>

                        <option value="Howell-Oregon Electric Coop">
                            Howell-Oregon Electric Coop
                        </option>

                        <option value="Independence Power & Light">
                            Independence Power & Light
                        </option>

                        <option value="Intercounty Electric Coop (IECA)">
                            Intercounty Electric Coop (IECA)
                        </option>

                        <option value=
                        "KCP&L Greater Missouri Operations Co (GMO)">
                            KCP&L Greater Missouri Operations Co (GMO)
                        </option>

                        <option value="Kansas City Power & Light (KCP&L)">
                            Kansas City Power & Light (KCP&L)
                        </option>

                        <option value="Laclede Electric Coop">
                            Laclede Electric Coop
                        </option>

                        <option value="Memphis">
                            Memphis
                        </option>

                        <option value="New-Mac Electric">
                            New-Mac Electric
                        </option>

                        <option value="Odessa">
                            Odessa
                        </option>

                        <option value="Osage Valley Electric Coop">
                            Osage Valley Electric Coop
                        </option>

                        <option value="Ozark Border Electric Coop">
                            Ozark Border Electric Coop
                        </option>

                        <option value="Ozark Electric Coop">
                            Ozark Electric Coop
                        </option>

                        <option value="Platte-Clay Electric Coop (PCEC)">
                            Platte-Clay Electric Coop (PCEC)
                        </option>

                        <option value="SEMO Electric Coop">
                            SEMO Electric Coop
                        </option>

                        <option value="Sac-Osage Electric Coop">
                            Sac-Osage Electric Coop
                        </option>

                        <option value="Shelbina">
                            Shelbina
                        </option>

                        <option value=
                        "Sikeston Board of Municipal Utilities (BMU)">
                            Sikeston Board of Municipal Utilities (BMU)
                        </option>

                        <option value="Southwest Electric Coop (SWEC)">
                            Southwest Electric Coop (SWEC)
                        </option>

                        <option value="Three Rivers Electric Coop">
                            Three Rivers Electric Coop
                        </option>

                        <option value="Tri-County Electric Coop">
                            Tri-County Electric Coop
                        </option>

                        <option value="Union Electric Co (Ameren)">
                            Union Electric Co (Ameren)
                        </option>

                        <option value="West Central Electric Coop">
                            West Central Electric Coop
                        </option>

                        <option value=
                        "White River Valley Electric Coop (WRVEC)">
                            White River Valley Electric Coop (WRVEC)
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-MS">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="4-County Electric Power Association">
                            4-County Electric Power Association
                        </option>

                        <option value=
                        "Alcorn County Electric Power Association">
                            Alcorn County Electric Power Association
                        </option>

                        <option value="Central Electric Power Association">
                            Central Electric Power Association
                        </option>

                        <option value="Coast Electric Power Association">
                            Coast Electric Power Association
                        </option>

                        <option value=
                        "Dixie Electric Power Assocation (Dixie EPA)">
                            Dixie Electric Power Assocation (Dixie EPA)
                        </option>

                        <option value="Dixie Power">
                            Dixie Power
                        </option>

                        <option value=
                        "East Mississippi Electric Power Association (EMEPA)">
                            East Mississippi Electric Power Association (EMEPA)
                        </option>

                        <option value="Entergy Mississippi">
                            Entergy Mississippi
                        </option>

                        <option value="Magnolia Electric Power Association">
                            Magnolia Electric Power Association
                        </option>

                        <option value="Mississippi Power Co">
                            Mississippi Power Co
                        </option>

                        <option value=
                        "Monroe County Electric Power Association">
                            Monroe County Electric Power Association
                        </option>

                        <option value="Natchez Trace Electric Power">
                            Natchez Trace Electric Power
                        </option>

                        <option value=
                        "Northcentral Electric Power Association">
                            Northcentral Electric Power Association
                        </option>

                        <option value=
                        "Pearl River Valley Electric Power Association (PRVEPA)">
                        Pearl River Valley Electric Power Association (PRVEPA)
                        </option>

                        <option value=
                        "Singing River Electric Power Association">
                            Singing River Electric Power Association
                        </option>

                        <option value=
                        "Southern Pine Electric Power Association (SPEPA)">
                            Southern Pine Electric Power Association (SPEPA)
                        </option>

                        <option value=
                        "Southwest Mississippi Electric Power Association">
                            Southwest Mississippi Electric Power Association
                        </option>

                        <option value=
                        "Tallahatchie Valley Electric Power Association (TVEPA)">
                        Tallahatchie Valley Electric Power Association (TVEPA)
                        </option>

                        <option value="Tennessee Valley Authority (TVA)">
                            Tennessee Valley Authority (TVA)
                        </option>

                        <option value="Tishomingo County EPA (TCEPA)">
                            Tishomingo County EPA (TCEPA)
                        </option>

                        <option value="Tombigbee Electric Power Association">
                            Tombigbee Electric Power Association
                        </option>

                        <option value="Tupelo">
                            Tupelo
                        </option>

                        <option value=
                        "Yazoo Valley Electric Power Association">
                            Yazoo Valley Electric Power Association
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-MT">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Central Montana Electric Power Coop">
                            Central Montana Electric Power Coop
                        </option>

                        <option value="Flathead Electric Coop">
                            Flathead Electric Coop
                        </option>

                        <option value="Mission Valley Power">
                            Mission Valley Power
                        </option>

                        <option value="Montana-Dakota Utilities">
                            Montana-Dakota Utilities
                        </option>

                        <option value="Northwestern Energy">
                            Northwestern Energy
                        </option>

                        <option value="Ravalli County Electric Coop">
                            Ravalli County Electric Coop
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-NC">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Albemarle EMC">
                            Albemarle EMC
                        </option>

                        <option value="Apex">
                            Apex
                        </option>

                        <option value="Ayden">
                            Ayden
                        </option>

                        <option value="Blue Ridge EMC (BREMCO)">
                            Blue Ridge EMC (BREMCO)
                        </option>

                        <option value="Brunswick EMC">
                            Brunswick EMC
                        </option>

                        <option value="Carteret-Craven Electric Coop">
                            Carteret-Craven Electric Coop
                        </option>

                        <option value="Concord">
                            Concord
                        </option>

                        <option value="Direct Energy">
                            Direct Energy
                        </option>

                        <option value="Duke Energy">
                            Duke Energy
                        </option>

                        <option value="Edgecombe-Martin County EMC">
                            Edgecombe-Martin County EMC
                        </option>

                        <option value="Electricities">
                            Electricities
                        </option>

                        <option value="EnergyUnited">
                            EnergyUnited
                        </option>

                        <option value="Fayetteville PWC">
                            Fayetteville PWC
                        </option>

                        <option value="Four County EMC">
                            Four County EMC
                        </option>

                        <option value="French Broad EMC">
                            French Broad EMC
                        </option>

                        <option value="Gastonia">
                            Gastonia
                        </option>

                        <option value="Granite Falls">
                            Granite Falls
                        </option>

                        <option value="Greenville Utilities Commission (GUC)">
                            Greenville Utilities Commission (GUC)
                        </option>

                        <option value="Hamilton">
                            Hamilton
                        </option>

                        <option value="Haywood EMC">
                            Haywood EMC
                        </option>

                        <option value="High Point">
                            High Point
                        </option>

                        <option value="Jones-Onslow EMC (JOEMC)">
                            Jones-Onslow EMC (JOEMC)
                        </option>

                        <option value="Louisburg">
                            Louisburg
                        </option>

                        <option value="Lumbee River EMC">
                            Lumbee River EMC
                        </option>

                        <option value="Monroe">
                            Monroe
                        </option>

                        <option value="New Bern">
                            New Bern
                        </option>

                        <option value="North Carolina Power (Dominion)">
                            North Carolina Power (Dominion)
                        </option>

                        <option value="Pee Dee EMC (PDEMC)">
                            Pee Dee EMC (PDEMC)
                        </option>

                        <option value="Piedmont EMC">
                            Piedmont EMC
                        </option>

                        <option value="Progress Energy Carolinas">
                            Progress Energy Carolinas
                        </option>

                        <option value="Randolph EMC">
                            Randolph EMC
                        </option>

                        <option value="Roanoke EMC">
                            Roanoke EMC
                        </option>

                        <option value="Rocky Mount Public Utilities">
                            Rocky Mount Public Utilities
                        </option>

                        <option value=
                        "Rutherford Electric Membership Corp (REMC)">
                            Rutherford Electric Membership Corp (REMC)
                        </option>

                        <option value="South River EMC (SREMC)">
                            South River EMC (SREMC)
                        </option>

                        <option value="Statesville">
                            Statesville
                        </option>

                        <option value="Surry-Yadkin EMC (SYEMC)">
                            Surry-Yadkin EMC (SYEMC)
                        </option>

                        <option value="Tideland EMC">
                            Tideland EMC
                        </option>

                        <option value="Tri-County EMC (TCEMC)">
                            Tri-County EMC (TCEMC)
                        </option>

                        <option value="Tri-State EMC">
                            Tri-State EMC
                        </option>

                        <option value="Union Power Coop">
                            Union Power Coop
                        </option>

                        <option value="Wake Electric (WEMC)">
                            Wake Electric (WEMC)
                        </option>

                        <option value="Wilson">
                            Wilson
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-ND">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Basin Electric Power Coop">
                            Basin Electric Power Coop
                        </option>

                        <option value="Cass County Electric Coop (CCEC)">
                            Cass County Electric Coop (CCEC)
                        </option>

                        <option value="Central Power Electric Coop">
                            Central Power Electric Coop
                        </option>

                        <option value="Montana-Dakota Utilities">
                            Montana-Dakota Utilities
                        </option>

                        <option value="Mountrail-Williams Electric Coop">
                            Mountrail-Williams Electric Coop
                        </option>

                        <option value="Nodak Electric Coop">
                            Nodak Electric Coop
                        </option>

                        <option value="Otter Tail Power Co">
                            Otter Tail Power Co
                        </option>

                        <option value="Valley City">
                            Valley City
                        </option>

                        <option value="Verendrye Electric Coop">
                            Verendrye Electric Coop
                        </option>

                        <option value="Xcel Energy">
                            Xcel Energy
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-NE">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Dawson Public Power District">
                            Dawson Public Power District
                        </option>

                        <option value=
                        "Grand Island Utilities Department (GIUD)">
                            Grand Island Utilities Department (GIUD)
                        </option>

                        <option value="Lincoln Electric System (LES)">
                            Lincoln Electric System (LES)
                        </option>

                        <option value="Nebraska Public Power District (NPPD)">
                            Nebraska Public Power District (NPPD)
                        </option>

                        <option value="Omaha Public Power District (OPPD)">
                            Omaha Public Power District (OPPD)
                        </option>

                        <option value="Panhandle Rural Electric">
                            Panhandle Rural Electric
                        </option>

                        <option value="Roosevelt Public Power District (RPPD)">
                            Roosevelt Public Power District (RPPD)
                        </option>

                        <option value="South Central Public Power District">
                            South Central Public Power District
                        </option>

                        <option value="Southern Power District">
                            Southern Power District
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-NH">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value=
                        "Granite State Electric (Liberty Utilities)">
                            Granite State Electric (Liberty Utilities)
                        </option>

                        <option value="National Grid">
                            National Grid
                        </option>

                        <option value="New Hampshire Electric Coop (NHEC)">
                            New Hampshire Electric Coop (NHEC)
                        </option>

                        <option value="Public Service of NH (PSNH)">
                            Public Service of NH (PSNH)
                        </option>

                        <option value="Unitil Energy Systems">
                            Unitil Energy Systems
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-NJ">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Atlantic City Electric">
                            Atlantic City Electric
                        </option>

                        <option value="Borough of Milltown">
                            Borough of Milltown
                        </option>

                        <option value="Butler">
                            Butler
                        </option>

                        <option value="Champion Energy">
                            Champion Energy
                        </option>

                        <option value="Direct Energy">
                            Direct Energy
                        </option>

                        <option value=
                        "Jersey Central Power & Light (JCP&L, FirstEnergy)">
                            Jersey Central Power & Light (JCP&L, FirstEnergy)
                        </option>

                        <option value="Madison">
                            Madison
                        </option>

                        <option value="Park Ridge">
                            Park Ridge
                        </option>

                        <option value="Public Service Electric & Gas (PSE&G)">
                            Public Service Electric & Gas (PSE&G)
                        </option>

                        <option value="Rockland Electric Co (RECO)">
                            Rockland Electric Co (RECO)
                        </option>

                        <option value="South River">
                            South River
                        </option>

                        <option value="Starion Energy">
                            Starion Energy
                        </option>

                        <option value="Sussex REC">
                            Sussex REC
                        </option>

                        <option value=
                        "Vineland Municipal Electric Utility (VMEU/VMU)">
                            Vineland Municipal Electric Utility (VMEU/VMU)
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-NM">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Central Valley Electric Coop">
                            Central Valley Electric Coop
                        </option>

                        <option value=
                        "Continental Divide Electric Coop (CDEC)">
                            Continental Divide Electric Coop (CDEC)
                        </option>

                        <option value="El Paso Electric (EP Electric)">
                            El Paso Electric (EP Electric)
                        </option>

                        <option value="Farmers Electric Coop">
                            Farmers Electric Coop
                        </option>

                        <option value=
                        "Farmington Electric Utility Service (FEUS)">
                            Farmington Electric Utility Service (FEUS)
                        </option>

                        <option value="Jemez Mountains Electric Coop">
                            Jemez Mountains Electric Coop
                        </option>

                        <option value="Kit Carson Electric Coop (KCEC)">
                            Kit Carson Electric Coop (KCEC)
                        </option>

                        <option value="Lea County Electric Coop">
                            Lea County Electric Coop
                        </option>

                        <option value="Navajo Tribal Utility Authority (NTUA)">
                            Navajo Tribal Utility Authority (NTUA)
                        </option>

                        <option value=
                        "Northern Rio Arriba Electric Coop (NORA)">
                            Northern Rio Arriba Electric Coop (NORA)
                        </option>

                        <option value="Otero County Electric Coop">
                            Otero County Electric Coop
                        </option>

                        <option value="Public Service Co of NM (PNM)">
                            Public Service Co of NM (PNM)
                        </option>

                        <option value="Roosevelt County Electric Coop">
                            Roosevelt County Electric Coop
                        </option>

                        <option value="Sierra Electric Coop">
                            Sierra Electric Coop
                        </option>

                        <option value="Springer Electric Coop">
                            Springer Electric Coop
                        </option>

                        <option value="Tri-County Electric Coop (TCEC)">
                            Tri-County Electric Coop (TCEC)
                        </option>

                        <option value="Truth of Consequences">
                            Truth of Consequences
                        </option>

                        <option value="Xcel Energy">
                            Xcel Energy
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-NV">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Boulder">
                            Boulder
                        </option>

                        <option value="Lincoln County PUD No 1">
                            Lincoln County PUD No 1
                        </option>

                        <option value="Mt Wheeler Power">
                            Mt Wheeler Power
                        </option>

                        <option value="NV Energy">
                            NV Energy
                        </option>

                        <option value="Overton Power District 5">
                            Overton Power District 5
                        </option>

                        <option value="Plumas-Sierra Rural Electric Coop">
                            Plumas-Sierra Rural Electric Coop
                        </option>

                        <option value="Raft River Rural Electric">
                            Raft River Rural Electric
                        </option>

                        <option value="Valley Electric Association (VEA)">
                            Valley Electric Association (VEA)
                        </option>

                        <option value="Wells Rural Electric Co">
                            Wells Rural Electric Co
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-NY">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Akron">
                            Akron
                        </option>

                        <option value="Arcade">
                            Arcade
                        </option>

                        <option value="Bath Electric Gas & Water (BEGWS)">
                            Bath Electric Gas & Water (BEGWS)
                        </option>

                        <option value="Boonville">
                            Boonville
                        </option>

                        <option value="Bounce Energy">
                            Bounce Energy
                        </option>

                        <option value="Calpine Power America">
                            Calpine Power America
                        </option>

                        <option value="Central Hudson Gas & Electric">
                            Central Hudson Gas & Electric
                        </option>

                        <option value="Churchville">
                            Churchville
                        </option>

                        <option value="Consolidated Edison-NY (ConEd)">
                            Consolidated Edison-NY (ConEd)
                        </option>

                        <option value="Delaware County Electric Coop">
                            Delaware County Electric Coop
                        </option>

                        <option value="Direct Energy">
                            Direct Energy
                        </option>

                        <option value="Eastern Maine Electric Coop">
                            Eastern Maine Electric Coop
                        </option>

                        <option value="Endicott">
                            Endicott
                        </option>

                        <option value="Fairport Electric">
                            Fairport Electric
                        </option>

                        <option value="Freeport Electric">
                            Freeport Electric
                        </option>

                        <option value="Green Island (GIPA)">
                            Green Island (GIPA)
                        </option>

                        <option value="Green Mountain Energy">
                            Green Mountain Energy
                        </option>

                        <option value="Groton">
                            Groton
                        </option>

                        <option value="Hamilton">
                            Hamilton
                        </option>

                        <option value="Holley">
                            Holley
                        </option>

                        <option value="Ilion">
                            Ilion
                        </option>

                        <option value=
                        "Jamestown Board of Public Utilities (BPU)">
                            Jamestown Board of Public Utilities (BPU)
                        </option>

                        <option value="Kiwi Energy">
                            Kiwi Energy
                        </option>

                        <option value="Lake Placid">
                            Lake Placid
                        </option>

                        <option value="Massena">
                            Massena
                        </option>

                        <option value="National Grid">
                            National Grid
                        </option>

                        <option value="New York State Electric & Gas (NYSEG)">
                            New York State Electric & Gas (NYSEG)
                        </option>

                        <option value="Oneida-Madison Electric Coop">
                            Oneida-Madison Electric Coop
                        </option>

                        <option value="Orange & Rockland Utilities (ORU)">
                            Orange & Rockland Utilities (ORU)
                        </option>

                        <option value="Otsego Electric Coop">
                            Otsego Electric Coop
                        </option>

                        <option value="PSEG Long Island">
                            PSEG Long Island
                        </option>

                        <option value="Pennsylvania Electric (Penelec)">
                            Pennsylvania Electric (Penelec)
                        </option>

                        <option value="Richmondville">
                            Richmondville
                        </option>

                        <option value="Rochester Gas & Electric (RG&E)">
                            Rochester Gas & Electric (RG&E)
                        </option>

                        <option value="Salamanca">
                            Salamanca
                        </option>

                        <option value="Spencerport Municipal Electric (SME)">
                            Spencerport Municipal Electric (SME)
                        </option>

                        <option value="Starion Energy">
                            Starion Energy
                        </option>

                        <option value="Steuben REC">
                            Steuben REC
                        </option>

                        <option value="Sussex REC">
                            Sussex REC
                        </option>

                        <option value="Tupper Lake">
                            Tupper Lake
                        </option>

                        <option value="Wellsville">
                            Wellsville
                        </option>

                        <option value="Westfield">
                            Westfield
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-OH">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="AEP Ohio">
                            AEP Ohio
                        </option>

                        <option value="Adams REC">
                            Adams REC
                        </option>

                        <option value="Bowling Green">
                            Bowling Green
                        </option>

                        <option value="Buckeye REC">
                            Buckeye REC
                        </option>

                        <option value="Butler REC">
                            Butler REC
                        </option>

                        <option value="Cincinnati Bell Energy">
                            Cincinnati Bell Energy
                        </option>

                        <option value="Cleveland Public Power (CPP)">
                            Cleveland Public Power (CPP)
                        </option>

                        <option value="Consolidated Electric Coop">
                            Consolidated Electric Coop
                        </option>

                        <option value="Cuyahoga Falls">
                            Cuyahoga Falls
                        </option>

                        <option value="Dayton Power & Light (DP&L)">
                            Dayton Power & Light (DP&L)
                        </option>

                        <option value="Direct Energy">
                            Direct Energy
                        </option>

                        <option value="Dover">
                            Dover
                        </option>

                        <option value="Duke Energy">
                            Duke Energy
                        </option>

                        <option value="FirstEnergy">
                            FirstEnergy
                        </option>

                        <option value="Frontier Power Co">
                            Frontier Power Co
                        </option>

                        <option value="Hamilton">
                            Hamilton
                        </option>

                        <option value="Hancock-Wood Electric Coop">
                            Hancock-Wood Electric Coop
                        </option>

                        <option value="Hudson">
                            Hudson
                        </option>

                        <option value="Jackson">
                            Jackson
                        </option>

                        <option value="Licking Rural Electrification (LRE)">
                            Licking Rural Electrification (LRE)
                        </option>

                        <option value="Lorain-Medina REC">
                            Lorain-Medina REC
                        </option>

                        <option value="Napoleon Power & Light">
                            Napoleon Power & Light
                        </option>

                        <option value="North Central Electric Coop (NCELEC)">
                            North Central Electric Coop (NCELEC)
                        </option>

                        <option value="Ohio Edison (FirstEnergy)">
                            Ohio Edison (FirstEnergy)
                        </option>

                        <option value="Paulding-Putnam ECA">
                            Paulding-Putnam ECA
                        </option>

                        <option value="Piqua">
                            Piqua
                        </option>

                        <option value="Shelby">
                            Shelby
                        </option>

                        <option value="South Central Power Co">
                            South Central Power Co
                        </option>

                        <option value="Starion Energy">
                            Starion Energy
                        </option>

                        <option value="The Illuminating Company (Cleveland)">
                            The Illuminating Company (Cleveland)
                        </option>

                        <option value="Toledo Energy (FirstEnergy)">
                            Toledo Energy (FirstEnergy)
                        </option>

                        <option value="Tricounty REC">
                            Tricounty REC
                        </option>

                        <option value="Union REC">
                            Union REC
                        </option>

                        <option value="Vectren Corp">
                            Vectren Corp
                        </option>

                        <option value="Washington Electric Coop">
                            Washington Electric Coop
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-OK">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Caddo Electric Coop">
                            Caddo Electric Coop
                        </option>

                        <option value="Canadian Valley Electric Coop (CVEC)">
                            Canadian Valley Electric Coop (CVEC)
                        </option>

                        <option value="Central Rural Electric Coop (CREC)">
                            Central Rural Electric Coop (CREC)
                        </option>

                        <option value="Choctaw Electric Coop">
                            Choctaw Electric Coop
                        </option>

                        <option value="Cotton Electric Coop">
                            Cotton Electric Coop
                        </option>

                        <option value="Direct Energy">
                            Direct Energy
                        </option>

                        <option value=
                        "East Central Oklahoma Electric Coop (ECOEC)">
                            East Central Oklahoma Electric Coop (ECOEC)
                        </option>

                        <option value="Edmond Electric">
                            Edmond Electric
                        </option>

                        <option value="Kiamichi Electric Coop">
                            Kiamichi Electric Coop
                        </option>

                        <option value="Kingfisher">
                            Kingfisher
                        </option>

                        <option value="Lake Region Electric Coop (LREC)">
                            Lake Region Electric Coop (LREC)
                        </option>

                        <option value="Newkirk">
                            Newkirk
                        </option>

                        <option value="Northeast Oklahoma Electric Coop">
                            Northeast Oklahoma Electric Coop
                        </option>

                        <option value="Northwestern Electric Coop (NWECOK)">
                            Northwestern Electric Coop (NWECOK)
                        </option>

                        <option value="Oklahoma Electric Coop">
                            Oklahoma Electric Coop
                        </option>

                        <option value="Oklahoma Gas & Electric (OGE)">
                            Oklahoma Gas & Electric (OGE)
                        </option>

                        <option value="People's Electric Coop">
                            People's Electric Coop
                        </option>

                        <option value="Pond Creek">
                            Pond Creek
                        </option>

                        <option value=
                        "Public Service Company Of Oklahoma (PSO)">
                            Public Service Company Of Oklahoma (PSO)
                        </option>

                        <option value="Red River Valley REA">
                            Red River Valley REA
                        </option>

                        <option value="Tri-County Electric Coop (TCEC)">
                            Tri-County Electric Coop (TCEC)
                        </option>

                        <option value="Verdigris Valley Electric Coop (VVEC)">
                            Verdigris Valley Electric Coop (VVEC)
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-OR">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Ashland Electric Department">
                            Ashland Electric Department
                        </option>

                        <option value="Avista">
                            Avista
                        </option>

                        <option value="Bandon">
                            Bandon
                        </option>

                        <option value="Blachly-Lane County Coop">
                            Blachly-Lane County Coop
                        </option>

                        <option value="Canby">
                            Canby
                        </option>

                        <option value="Cascade Locks">
                            Cascade Locks
                        </option>

                        <option value="Central Electric Coop (CEC)">
                            Central Electric Coop (CEC)
                        </option>

                        <option value=
                        "Central Lincoln People's Utility District (CLPUD)">
                            Central Lincoln People's Utility District (CLPUD)
                        </option>

                        <option value="Columbia Basin Electric Coop">
                            Columbia Basin Electric Coop
                        </option>

                        <option value="Columbia River PUD">
                            Columbia River PUD
                        </option>

                        <option value="Consumers Power Inc (CPI)">
                            Consumers Power Inc (CPI)
                        </option>

                        <option value="Coos Curry Electric Coop (CCEC)">
                            Coos Curry Electric Coop (CCEC)
                        </option>

                        <option value="Douglas Electric Coop">
                            Douglas Electric Coop
                        </option>

                        <option value=
                        "Emerald People's Utility District (EPUD)">
                            Emerald People's Utility District (EPUD)
                        </option>

                        <option value="Eugene Water & Electric Board (EWEB)">
                            Eugene Water & Electric Board (EWEB)
                        </option>

                        <option value="Forest Grove">
                            Forest Grove
                        </option>

                        <option value="Green Mountain Energy">
                            Green Mountain Energy
                        </option>

                        <option value="Hermiston">
                            Hermiston
                        </option>

                        <option value="Hood River Electric Coop">
                            Hood River Electric Coop
                        </option>

                        <option value="Lane Electric">
                            Lane Electric
                        </option>

                        <option value="McMinnville">
                            McMinnville
                        </option>

                        <option value="Midstate Electric Coop">
                            Midstate Electric Coop
                        </option>

                        <option value="Monmouth">
                            Monmouth
                        </option>

                        <option value="Oregon Trail Electric Coop (OTECC)">
                            Oregon Trail Electric Coop (OTECC)
                        </option>

                        <option value="Pacific Power (PacifiCorp)">
                            Pacific Power (PacifiCorp)
                        </option>

                        <option value="Portland General Electric (PGE)">
                            Portland General Electric (PGE)
                        </option>

                        <option value="Salem Electric">
                            Salem Electric
                        </option>

                        <option value="Springfield Utility Board (SUB)">
                            Springfield Utility Board (SUB)
                        </option>

                        <option value="Tillamook PUD">
                            Tillamook PUD
                        </option>

                        <option value="Umatilla Electric">
                            Umatilla Electric
                        </option>

                        <option value="Wasco Electric Coop">
                            Wasco Electric Coop
                        </option>

                        <option value="West Oregon Electric Coop">
                            West Oregon Electric Coop
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-PA">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Adams Electric Coop (Adams EC)">
                            Adams Electric Coop (Adams EC)
                        </option>

                        <option value="Bedford REC">
                            Bedford REC
                        </option>

                        <option value="Bounce Energy">
                            Bounce Energy
                        </option>

                        <option value="CPL Retail Energy">
                            CPL Retail Energy
                        </option>

                        <option value="Central Electric Coop">
                            Central Electric Coop
                        </option>

                        <option value="Chambersburg">
                            Chambersburg
                        </option>

                        <option value="Champion Energy">
                            Champion Energy
                        </option>

                        <option value="Citizens Electric of Lewisburg">
                            Citizens Electric of Lewisburg
                        </option>

                        <option value="Direct Energy">
                            Direct Energy
                        </option>

                        <option value="Duquesne Light (DQE)">
                            Duquesne Light (DQE)
                        </option>

                        <option value="Green Mountain Energy">
                            Green Mountain Energy
                        </option>

                        <option value="Metropolitan Edison (Met-Ed)">
                            Metropolitan Edison (Met-Ed)
                        </option>

                        <option value="Northwestern Rural ECA">
                            Northwestern Rural ECA
                        </option>

                        <option value="PECO Energy">
                            PECO Energy
                        </option>

                        <option value="PPL Electric Utilities (PPL)">
                            PPL Electric Utilities (PPL)
                        </option>

                        <option value="Penn Power (FirstEnergy)">
                            Penn Power (FirstEnergy)
                        </option>

                        <option value="Pennsylvania Electric (Penelec)">
                            Pennsylvania Electric (Penelec)
                        </option>

                        <option value="Pike County Light & Power Co (PCL&P)">
                            Pike County Light & Power Co (PCL&P)
                        </option>

                        <option value="REA Energy Coop">
                            REA Energy Coop
                        </option>

                        <option value="Somerset REC">
                            Somerset REC
                        </option>

                        <option value="Starion Energy">
                            Starion Energy
                        </option>

                        <option value="UGI Utilities">
                            UGI Utilities
                        </option>

                        <option value="United Electric Coop">
                            United Electric Coop
                        </option>

                        <option value="Valley Rural (Valley REC)">
                            Valley Rural (Valley REC)
                        </option>

                        <option value="Washington Gas Energy Services (WGES)">
                            Washington Gas Energy Services (WGES)
                        </option>

                        <option value="Wellsboro Electric Co">
                            Wellsboro Electric Co
                        </option>

                        <option value="West Penn Power (FirstEnergy)">
                            West Penn Power (FirstEnergy)
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-RI">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="National Grid">
                            National Grid
                        </option>

                        <option value="Pascoag Utility District (PUD)">
                            Pascoag Utility District (PUD)
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-SC">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Aiken Electric Coop">
                            Aiken Electric Coop
                        </option>

                        <option value="Berkeley Electric Coop (BECSC)">
                            Berkeley Electric Coop (BECSC)
                        </option>

                        <option value="Black River Electric Coop">
                            Black River Electric Coop
                        </option>

                        <option value="Blue Ridge Electric Coop">
                            Blue Ridge Electric Coop
                        </option>

                        <option value="Broad River Electric Coop">
                            Broad River Electric Coop
                        </option>

                        <option value="Camden">
                            Camden
                        </option>

                        <option value="Central Electric Power Coop">
                            Central Electric Power Coop
                        </option>

                        <option value="Clinton Combined Utility System">
                            Clinton Combined Utility System
                        </option>

                        <option value="Coastal Electric Coop">
                            Coastal Electric Coop
                        </option>

                        <option value="Duke Energy">
                            Duke Energy
                        </option>

                        <option value="Easley Combined Utilities">
                            Easley Combined Utilities
                        </option>

                        <option value="Edisto Electric Coop">
                            Edisto Electric Coop
                        </option>

                        <option value="Fairfield Electric Coop">
                            Fairfield Electric Coop
                        </option>

                        <option value="Greer">
                            Greer
                        </option>

                        <option value="Horry Electric Coop (HEC)">
                            Horry Electric Coop (HEC)
                        </option>

                        <option value="Laurens Electric Coop">
                            Laurens Electric Coop
                        </option>

                        <option value="Lynches River Electric Coop">
                            Lynches River Electric Coop
                        </option>

                        <option value="Marlboro Electric Coop">
                            Marlboro Electric Coop
                        </option>

                        <option value="Mid-Carolina Electric Coop (MCEC)">
                            Mid-Carolina Electric Coop (MCEC)
                        </option>

                        <option value=
                        "Orangeburg Department of Public Utilities (ORBGDPU)">
                            Orangeburg Department of Public Utilities (ORBGDPU)
                        </option>

                        <option value="Palmetto Electric Coop">
                            Palmetto Electric Coop
                        </option>

                        <option value="Pee Dee Electric Coop">
                            Pee Dee Electric Coop
                        </option>

                        <option value="Progress Energy Carolinas">
                            Progress Energy Carolinas
                        </option>

                        <option value="Rock Hill">
                            Rock Hill
                        </option>

                        <option value="Santee Cooper Power">
                            Santee Cooper Power
                        </option>

                        <option value="Santee Electric Coop">
                            Santee Electric Coop
                        </option>

                        <option value="Seneca">
                            Seneca
                        </option>

                        <option value="South Carolina Electric & Gas (SCE&G)">
                            South Carolina Electric & Gas (SCE&G)
                        </option>

                        <option value="Tri-County Electric Coop (TCE)">
                            Tri-County Electric Coop (TCE)
                        </option>

                        <option value="Union">
                            Union
                        </option>

                        <option value="York Electric Coop">
                            York Electric Coop
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-SD">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Aurora">
                            Aurora
                        </option>

                        <option value="Black Hills Power">
                            Black Hills Power
                        </option>

                        <option value="East River Electric Power Coop">
                            East River Electric Power Coop
                        </option>

                        <option value="Montana-Dakota Utilities">
                            Montana-Dakota Utilities
                        </option>

                        <option value="Northwestern Energy">
                            Northwestern Energy
                        </option>

                        <option value="Southeastern Electric Coop">
                            Southeastern Electric Coop
                        </option>

                        <option value="West River Electric Association">
                            West River Electric Association
                        </option>

                        <option value="Xcel Energy">
                            Xcel Energy
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-TN">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="American Electric Power (AEP)">
                            American Electric Power (AEP)
                        </option>

                        <option value="Appalachian Electric Coop">
                            Appalachian Electric Coop
                        </option>

                        <option value="Appalachian Power (AEP)">
                            Appalachian Power (AEP)
                        </option>

                        <option value=
                        "Bristol Tennessee Essential Services (BTES)">
                            Bristol Tennessee Essential Services (BTES)
                        </option>

                        <option value="Caney Fork Electric Coop">
                            Caney Fork Electric Coop
                        </option>

                        <option value="Chattanooga (EPB)">
                            Chattanooga (EPB)
                        </option>

                        <option value=
                        "Clarksville Department of Electricity (CDE)">
                            Clarksville Department of Electricity (CDE)
                        </option>

                        <option value="Cleveland Utilities">
                            Cleveland Utilities
                        </option>

                        <option value="Clinton Utilities Board">
                            Clinton Utilities Board
                        </option>

                        <option value="Columbia Power System">
                            Columbia Power System
                        </option>

                        <option value="Cumberland EMC">
                            Cumberland EMC
                        </option>

                        <option value="Dayton">
                            Dayton
                        </option>

                        <option value="Dickson Electric Systems">
                            Dickson Electric Systems
                        </option>

                        <option value="Duck River EMC">
                            Duck River EMC
                        </option>

                        <option value="Elizabethton Electric Department (EED)">
                            Elizabethton Electric Department (EED)
                        </option>

                        <option value="Erwin">
                            Erwin
                        </option>

                        <option value="Fayetteville">
                            Fayetteville
                        </option>

                        <option value="Forked Deer Electric Coop">
                            Forked Deer Electric Coop
                        </option>

                        <option value="Fort Loudoun Electric Coop (FLEC)">
                            Fort Loudoun Electric Coop (FLEC)
                        </option>

                        <option value="Gibson EMC">
                            Gibson EMC
                        </option>

                        <option value=
                        "Greeneville Light & Power System (GLPS)">
                            Greeneville Light & Power System (GLPS)
                        </option>

                        <option value="Harriman">
                            Harriman
                        </option>

                        <option value="Holston Electric Coop (HEC)">
                            Holston Electric Coop (HEC)
                        </option>

                        <option value="Jackson Energy Authority (JEA)">
                            Jackson Energy Authority (JEA)
                        </option>

                        <option value="Johnson City Power Board (JCPB)">
                            Johnson City Power Board (JCPB)
                        </option>

                        <option value="Knoxville Utilities Board (KUB)">
                            Knoxville Utilities Board (KUB)
                        </option>

                        <option value="Lafollette">
                            Lafollette
                        </option>

                        <option value="Lenoir City Utilities Board (LCUB)">
                            Lenoir City Utilities Board (LCUB)
                        </option>

                        <option value="Memphis Light Gas & Water (MLGW)">
                            Memphis Light Gas & Water (MLGW)
                        </option>

                        <option value="Meriwether Lewis Electric Coop (MLEC)">
                            Meriwether Lewis Electric Coop (MLEC)
                        </option>

                        <option value="Middle Tennessee EMC (MTEMC)">
                            Middle Tennessee EMC (MTEMC)
                        </option>

                        <option value="Mountain Electric Coop">
                            Mountain Electric Coop
                        </option>

                        <option value="Murfreesboro Electric Department">
                            Murfreesboro Electric Department
                        </option>

                        <option value="Nashville Electric Service (NES)">
                            Nashville Electric Service (NES)
                        </option>

                        <option value="Newport">
                            Newport
                        </option>

                        <option value="Pulaski">
                            Pulaski
                        </option>

                        <option value="Ripley">
                            Ripley
                        </option>

                        <option value="Rockwood">
                            Rockwood
                        </option>

                        <option value="Sequachee Valley Electric Coop (SVEC)">
                            Sequachee Valley Electric Coop (SVEC)
                        </option>

                        <option value="Sevier County Electric System (SCES)">
                            Sevier County Electric System (SCES)
                        </option>

                        <option value="Southwest Tennessee EMC (STEMC)">
                            Southwest Tennessee EMC (STEMC)
                        </option>

                        <option value="Springfield">
                            Springfield
                        </option>

                        <option value="Tallapoosa River Electric Coop">
                            Tallapoosa River Electric Coop
                        </option>

                        <option value="Tennessee Valley Authority (TVA)">
                            Tennessee Valley Authority (TVA)
                        </option>

                        <option value="Tri-County Electric (TCEMC)">
                            Tri-County Electric (TCEMC)
                        </option>

                        <option value="Tri-State EMC">
                            Tri-State EMC
                        </option>

                        <option value="Upper Cumberland EMC (UCEMC)">
                            Upper Cumberland EMC (UCEMC)
                        </option>

                        <option value="Volunteer Energy Coop (VEC)">
                            Volunteer Energy Coop (VEC)
                        </option>

                        <option value=
                        "Weakley County Municipal Electric System">
                            Weakley County Municipal Electric System
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-TX">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="4Change Energy Co">
                            4Change Energy Co
                        </option>

                        <option value="AEP Texas">
                            AEP Texas
                        </option>

                        <option value="APG&E">
                            APG&E
                        </option>

                        <option value="Acacia Energy">
                            Acacia Energy
                        </option>

                        <option value="Alliance Power Co">
                            Alliance Power Co
                        </option>

                        <option value="Ambit Energy">
                            Ambit Energy
                        </option>

                        <option value="American Light and Power">
                            American Light and Power
                        </option>

                        <option value="Amigo Energy">
                            Amigo Energy
                        </option>

                        <option value="Andeler Power">
                            Andeler Power
                        </option>

                        <option value="Atmos Energy">
                            Atmos Energy
                        </option>

                        <option value="Austin Energy">
                            Austin Energy
                        </option>

                        <option value="Bailey County Electric Coop">
                            Bailey County Electric Coop
                        </option>

                        <option value="Bandera Electric Coop">
                            Bandera Electric Coop
                        </option>

                        <option value="Bartlett Electric Coop">
                            Bartlett Electric Coop
                        </option>

                        <option value="Beyond Power">
                            Beyond Power
                        </option>

                        <option value="Bluebonnet Electric Coop">
                            Bluebonnet Electric Coop
                        </option>

                        <option value="Boerne">
                            Boerne
                        </option>

                        <option value="Bounce Energy">
                            Bounce Energy
                        </option>

                        <option value="Bowie-Cass Electric Coop">
                            Bowie-Cass Electric Coop
                        </option>

                        <option value="Brilliant Energy">
                            Brilliant Energy
                        </option>

                        <option value=
                        "Brownsville Public Utilities Board (BPUB)">
                            Brownsville Public Utilities Board (BPUB)
                        </option>

                        <option value="Burnet">
                            Burnet
                        </option>

                        <option value="CPL Retail Energy">
                            CPL Retail Energy
                        </option>

                        <option value="CPS Energy (City Public Service)">
                            CPS Energy (City Public Service)
                        </option>

                        <option value="CenterPoint Electric">
                            CenterPoint Electric
                        </option>

                        <option value="Central Texas Electric Coop (CTEC)">
                            Central Texas Electric Coop (CTEC)
                        </option>

                        <option value="Champion Energy">
                            Champion Energy
                        </option>

                        <option value="Cherokee County Electric Coop (CCECA)">
                            Cherokee County Electric Coop (CCECA)
                        </option>

                        <option value="Cirro Energy">
                            Cirro Energy
                        </option>

                        <option value="CoServ Electric">
                            CoServ Electric
                        </option>

                        <option value="Coleman County Electric Coop">
                            Coleman County Electric Coop
                        </option>

                        <option value="College Station">
                            College Station
                        </option>

                        <option value="Comanche Electric Coop">
                            Comanche Electric Coop
                        </option>

                        <option value="Cooke County Electric Coop (CCECA)">
                            Cooke County Electric Coop (CCECA)
                        </option>

                        <option value="DPI Energy">
                            DPI Energy
                        </option>

                        <option value="Deep East Texas Electric Coop">
                            Deep East Texas Electric Coop
                        </option>

                        <option value="Denton">
                            Denton
                        </option>

                        <option value="Direct Energy">
                            Direct Energy
                        </option>

                        <option value="El Paso Electric (EP Electric)">
                            El Paso Electric (EP Electric)
                        </option>

                        <option value="Entergy Texas">
                            Entergy Texas
                        </option>

                        <option value="Entrust Energy">
                            Entrust Energy
                        </option>

                        <option value="Everything Energy (NRG)">
                            Everything Energy (NRG)
                        </option>

                        <option value="Fannin County Electric Coop">
                            Fannin County Electric Coop
                        </option>

                        <option value="Farmers Electric Coop (FEC)">
                            Farmers Electric Coop (FEC)
                        </option>

                        <option value="Fayette Electric Coop">
                            Fayette Electric Coop
                        </option>

                        <option value="First Choice Power">
                            First Choice Power
                        </option>

                        <option value="Garland Power & Light Co (GP&L)">
                            Garland Power & Light Co (GP&L)
                        </option>

                        <option value="Georgetown">
                            Georgetown
                        </option>

                        <option value="Gexa Energy">
                            Gexa Energy
                        </option>

                        <option value="Goldthwaite">
                            Goldthwaite
                        </option>

                        <option value="Granbury">
                            Granbury
                        </option>

                        <option value="Grayson-Collin Electric Coop (GCEC)">
                            Grayson-Collin Electric Coop (GCEC)
                        </option>

                        <option value="Green Mountain Energy">
                            Green Mountain Energy
                        </option>

                        <option value="Greenbelt Electric Coop">
                            Greenbelt Electric Coop
                        </option>

                        <option value="Guadalupe Valley Electric Coop (GVEC)">
                            Guadalupe Valley Electric Coop (GVEC)
                        </option>

                        <option value="HILCO Electric Coop">
                            HILCO Electric Coop
                        </option>

                        <option value="Hamilton County Electric Coop">
                            Hamilton County Electric Coop
                        </option>

                        <option value="Houston County Electric Coop">
                            Houston County Electric Coop
                        </option>

                        <option value="Hudson Energy Services">
                            Hudson Energy Services
                        </option>

                        <option value="IGS Energy">
                            IGS Energy
                        </option>

                        <option value="Infinite Energy">
                            Infinite Energy
                        </option>

                        <option value="Jasper-Newton Electric Coop">
                            Jasper-Newton Electric Coop
                        </option>

                        <option value="Just Energy">
                            Just Energy
                        </option>

                        <option value="Karnes Electric Coop">
                            Karnes Electric Coop
                        </option>

                        <option value="Kerrville Public Utility Board (KPUB)">
                            Kerrville Public Utility Board (KPUB)
                        </option>

                        <option value="Lamar County Electric Coop">
                            Lamar County Electric Coop
                        </option>

                        <option value="Liberty Power">
                            Liberty Power
                        </option>

                        <option value="Lower Colorado River Authority">
                            Lower Colorado River Authority
                        </option>

                        <option value="Lubbock Power & Light (LP&L)">
                            Lubbock Power & Light (LP&L)
                        </option>

                        <option value="Luminant Energy">
                            Luminant Energy
                        </option>

                        <option value="Magic Valley Electric Coop">
                            Magic Valley Electric Coop
                        </option>

                        <option value="Medina Electric Coop">
                            Medina Electric Coop
                        </option>

                        <option value="Mid-South Electric Coop">
                            Mid-South Electric Coop
                        </option>

                        <option value="Navarro County Electric Coop">
                            Navarro County Electric Coop
                        </option>

                        <option value="Navasota Valley Electric Coop">
                            Navasota Valley Electric Coop
                        </option>

                        <option value="New Braunfels">
                            New Braunfels
                        </option>

                        <option value="Nueces Electric Coop">
                            Nueces Electric Coop
                        </option>

                        <option value="Oncor Electric Delivery Co">
                            Oncor Electric Delivery Co
                        </option>

                        <option value="Panola-Harrison Electric Coop">
                            Panola-Harrison Electric Coop
                        </option>

                        <option value="Pedernales Electric Coop (PEC)">
                            Pedernales Electric Coop (PEC)
                        </option>

                        <option value="Pennywise Power">
                            Pennywise Power
                        </option>

                        <option value="Potentia Energy">
                            Potentia Energy
                        </option>

                        <option value="Reliant">
                            Reliant
                        </option>

                        <option value="Rusk County Electric Coop">
                            Rusk County Electric Coop
                        </option>

                        <option value="Sam Houston Electric Coop">
                            Sam Houston Electric Coop
                        </option>

                        <option value=
                        "San Antonio City Public Service Energy (CPSE)">
                            San Antonio City Public Service Energy (CPSE)
                        </option>

                        <option value="San Bernard Electric Coop">
                            San Bernard Electric Coop
                        </option>

                        <option value="San Patricio Electric Coop">
                            San Patricio Electric Coop
                        </option>

                        <option value="Seguin">
                            Seguin
                        </option>

                        <option value="Sharyland Utilities">
                            Sharyland Utilities
                        </option>

                        <option value="South Plains Electric Coop">
                            South Plains Electric Coop
                        </option>

                        <option value=
                        "Southwestern Electric Power Co (SWEPCO)">
                            Southwestern Electric Power Co (SWEPCO)
                        </option>

                        <option value="Spark Energy">
                            Spark Energy
                        </option>

                        <option value="StarTex Power">
                            StarTex Power
                        </option>

                        <option value="Stream Energy">
                            Stream Energy
                        </option>

                        <option value="Summer Energy">
                            Summer Energy
                        </option>

                        <option value="TXU Energy">
                            TXU Energy
                        </option>

                        <option value="Tara Energy">
                            Tara Energy
                        </option>

                        <option value="Taylor Electric Coop">
                            Taylor Electric Coop
                        </option>

                        <option value="Texas-New Mexico Power Co">
                            Texas-New Mexico Power Co
                        </option>

                        <option value="Tri-County Electric Coop (TCE Texas)">
                            Tri-County Electric Coop (TCE Texas)
                        </option>

                        <option value="Tri-County Electric Coop (TCEC)">
                            Tri-County Electric Coop (TCEC)
                        </option>

                        <option value="TriEagle Energy">
                            TriEagle Energy
                        </option>

                        <option value="Trinity Valley Electric Coop (TVEC)">
                            Trinity Valley Electric Coop (TVEC)
                        </option>

                        <option value="TruSmart Energy">
                            TruSmart Energy
                        </option>

                        <option value="United Cooperative Services">
                            United Cooperative Services
                        </option>

                        <option value="Upshur RECC">
                            Upshur RECC
                        </option>

                        <option value="Veteran Energy">
                            Veteran Energy
                        </option>

                        <option value="Victoria Electric Coop">
                            Victoria Electric Coop
                        </option>

                        <option value="WTU Energy">
                            WTU Energy
                        </option>

                        <option value="Weatherford Municipal Utility System">
                            Weatherford Municipal Utility System
                        </option>

                        <option value="Wise Electric Coop">
                            Wise Electric Coop
                        </option>

                        <option value="Xcel Energy">
                            Xcel Energy
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-UT">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Bountiful">
                            Bountiful
                        </option>

                        <option value="Brigham City Public Power">
                            Brigham City Public Power
                        </option>

                        <option value="Dixie Power">
                            Dixie Power
                        </option>

                        <option value="Garkane Energy">
                            Garkane Energy
                        </option>

                        <option value="Heber Light & Power Co">
                            Heber Light & Power Co
                        </option>

                        <option value="Hurricane">
                            Hurricane
                        </option>

                        <option value="Hyrum City">
                            Hyrum City
                        </option>

                        <option value="Intermountain Power Agency">
                            Intermountain Power Agency
                        </option>

                        <option value="Kanosh">
                            Kanosh
                        </option>

                        <option value="Kaysville City">
                            Kaysville City
                        </option>

                        <option value="Lehi City">
                            Lehi City
                        </option>

                        <option value="Logan City Light & Power">
                            Logan City Light & Power
                        </option>

                        <option value="Moon Lake Electric Association">
                            Moon Lake Electric Association
                        </option>

                        <option value="Morgan City">
                            Morgan City
                        </option>

                        <option value="Murray City Power">
                            Murray City Power
                        </option>

                        <option value="Navajo Tribal Utility Authority (NTUA)">
                            Navajo Tribal Utility Authority (NTUA)
                        </option>

                        <option value="Nephi">
                            Nephi
                        </option>

                        <option value="Parowan">
                            Parowan
                        </option>

                        <option value="Payson">
                            Payson
                        </option>

                        <option value="Price">
                            Price
                        </option>

                        <option value="Provo City Power">
                            Provo City Power
                        </option>

                        <option value="Rocky Mountain Power (PacifiCorp)">
                            Rocky Mountain Power (PacifiCorp)
                        </option>

                        <option value="Santa Clara">
                            Santa Clara
                        </option>

                        <option value="Spanish Fork City Utility">
                            Spanish Fork City Utility
                        </option>

                        <option value="Springville City">
                            Springville City
                        </option>

                        <option value="St George Utilities">
                            St George Utilities
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-VA">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="A & N Electric Coop (ANEC)">
                            A & N Electric Coop (ANEC)
                        </option>

                        <option value="Appalachian Power (AEP)">
                            Appalachian Power (AEP)
                        </option>

                        <option value="Bristol Virginia Utilities (BVU)">
                            Bristol Virginia Utilities (BVU)
                        </option>

                        <option value="Central Virginia Electric Coop (CVEC)">
                            Central Virginia Electric Coop (CVEC)
                        </option>

                        <option value="Community Electric Coop">
                            Community Electric Coop
                        </option>

                        <option value="Danville City Power & Light">
                            Danville City Power & Light
                        </option>

                        <option value="Delmarva Power & Light (DPL)">
                            Delmarva Power & Light (DPL)
                        </option>

                        <option value="Dominion Virginia Power">
                            Dominion Virginia Power
                        </option>

                        <option value="Kentucky Utilities (KU)">
                            Kentucky Utilities (KU)
                        </option>

                        <option value="Manassas">
                            Manassas
                        </option>

                        <option value="Mecklenburg Electric Coop (MECELEC)">
                            Mecklenburg Electric Coop (MECELEC)
                        </option>

                        <option value="Northern Neck Electric Coop (NNEC)">
                            Northern Neck Electric Coop (NNEC)
                        </option>

                        <option value=
                        "Northern Virginia Electric Coop (NOVEC)">
                            Northern Virginia Electric Coop (NOVEC)
                        </option>

                        <option value="Potomac Edison (FirstEnergy)">
                            Potomac Edison (FirstEnergy)
                        </option>

                        <option value="Prince George Electric Coop">
                            Prince George Electric Coop
                        </option>

                        <option value="Rappahannock Electric Coop (REC)">
                            Rappahannock Electric Coop (REC)
                        </option>

                        <option value="Shenandoah Valley Electric Coop (SVEC)">
                            Shenandoah Valley Electric Coop (SVEC)
                        </option>

                        <option value="Southside Electric Coop (SEC)">
                            Southside Electric Coop (SEC)
                        </option>

                        <option value="Washington Gas Energy Services (WGES)">
                            Washington Gas Energy Services (WGES)
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-VT">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Green Mountain Power (GMP)">
                            Green Mountain Power (GMP)
                        </option>

                        <option value="Hardwick">
                            Hardwick
                        </option>

                        <option value="Vermont Electric Coop (VEC)">
                            Vermont Electric Coop (VEC)
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-WA">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Avista">
                            Avista
                        </option>

                        <option value="Benton PUD">
                            Benton PUD
                        </option>

                        <option value="Chelan PUD">
                            Chelan PUD
                        </option>

                        <option value="Clallam County PUD">
                            Clallam County PUD
                        </option>

                        <option value="Clark Public Utilities (CPU)">
                            Clark Public Utilities (CPU)
                        </option>

                        <option value="Cowlitz PUD">
                            Cowlitz PUD
                        </option>

                        <option value="Douglas County PUD">
                            Douglas County PUD
                        </option>

                        <option value="Franklin County PUD">
                            Franklin County PUD
                        </option>

                        <option value="Grant County PUD">
                            Grant County PUD
                        </option>

                        <option value="Grays Harbor PUD">
                            Grays Harbor PUD
                        </option>

                        <option value="Inland Power & Light">
                            Inland Power & Light
                        </option>

                        <option value="Klickitat County PUD">
                            Klickitat County PUD
                        </option>

                        <option value="Lewis County PUD No 1">
                            Lewis County PUD No 1
                        </option>

                        <option value="Mason County PUD 3">
                            Mason County PUD 3
                        </option>

                        <option value="Pacific Power (PacifiCorp)">
                            Pacific Power (PacifiCorp)
                        </option>

                        <option value="Port Angeles">
                            Port Angeles
                        </option>

                        <option value="Puget Sound Energy (PSE)">
                            Puget Sound Energy (PSE)
                        </option>

                        <option value="Richland">
                            Richland
                        </option>

                        <option value="Seattle Public Utilities">
                            Seattle Public Utilities
                        </option>

                        <option value="Snohomish County PUD">
                            Snohomish County PUD
                        </option>

                        <option value="Tacoma Power">
                            Tacoma Power
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-WI">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Adams-Columbia Electric Coop (ACECWI)">
                            Adams-Columbia Electric Coop (ACECWI)
                        </option>

                        <option value="Barron Electric Coop">
                            Barron Electric Coop
                        </option>

                        <option value="Benton">
                            Benton
                        </option>

                        <option value=
                        "Black River Falls Municipal Utilities (BRFMU)">
                            Black River Falls Municipal Utilities (BRFMU)
                        </option>

                        <option value="Clark Electric Coop">
                            Clark Electric Coop
                        </option>

                        <option value="Dahlberg Light & Power Co">
                            Dahlberg Light & Power Co
                        </option>

                        <option value="Dairyland Power Coop">
                            Dairyland Power Coop
                        </option>

                        <option value="Eagle River">
                            Eagle River
                        </option>

                        <option value="Gresham">
                            Gresham
                        </option>

                        <option value="Jackson Electric Coop">
                            Jackson Electric Coop
                        </option>

                        <option value="Jump River Electric Coop">
                            Jump River Electric Coop
                        </option>

                        <option value="Madison Gas & Electric (MGE)">
                            Madison Gas & Electric (MGE)
                        </option>

                        <option value="Marshfield">
                            Marshfield
                        </option>

                        <option value="New London">
                            New London
                        </option>

                        <option value=
                        "Northwestern Wisconsin Electric Co (NWECO)">
                            Northwestern Wisconsin Electric Co (NWECO)
                        </option>

                        <option value="Oakdale Electric Coop">
                            Oakdale Electric Coop
                        </option>

                        <option value="Pardeeville Public Utilities">
                            Pardeeville Public Utilities
                        </option>

                        <option value="Polk-Burnett Electric Coop">
                            Polk-Burnett Electric Coop
                        </option>

                        <option value="Rock Energy Cooperative">
                            Rock Energy Cooperative
                        </option>

                        <option value="St Croix Electric Coop">
                            St Croix Electric Coop
                        </option>

                        <option value=
                        "Superior Water, Light & Power (SWL&P; Allete)">
                            Superior Water, Light & Power (SWL&P; Allete)
                        </option>

                        <option value="WE Energies">
                            WE Energies
                        </option>

                        <option value="Wisconsin Power & Light (WPL; Alliant)">
                            Wisconsin Power & Light (WPL; Alliant)
                        </option>

                        <option value="Wisconsin Public Service (WPS)">
                            Wisconsin Public Service (WPS)
                        </option>

                        <option value="Xcel Energy">
                            Xcel Energy
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-WV">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Appalachian Power (AEP)">
                            Appalachian Power (AEP)
                        </option>

                        <option value="Mon Power (FirstEnergy)">
                            Mon Power (FirstEnergy)
                        </option>

                        <option value="Potomac Edison (FirstEnergy)">
                            Potomac Edison (FirstEnergy)
                        </option>

                        <option value="Wheeling Power Co (AEP)">
                            Wheeling Power Co (AEP)
                        </option>
                    </select>

                    <select data-validate="select" class="full electric_utility autoforward" id="electric_utility-WY">
                        <option value="">
                            Please Select
                        </option>

                        <option value="Other">
                            Other
                        </option>

                        <option value="Big Horn Electric Coop">
                            Big Horn Electric Coop
                        </option>

                        <option value="Bridger Valley Electric Association">
                            Bridger Valley Electric Association
                        </option>

                        <option value="Cheyenne Light Fuel & Power Co">
                            Cheyenne Light Fuel & Power Co
                        </option>

                        <option value="Cody">
                            Cody
                        </option>

                        <option value="High Plains Power">
                            High Plains Power
                        </option>

                        <option value="High West Energy">
                            High West Energy
                        </option>

                        <option value="Lower Valley Energy">
                            Lower Valley Energy
                        </option>

                        <option value="Montana-Dakota Utilities">
                            Montana-Dakota Utilities
                        </option>

                        <option value="Powder River Energy (PRE)">
                            Powder River Energy (PRE)
                        </option>

                        <option value="Rocky Mountain Power (PacifiCorp)">
                            Rocky Mountain Power (PacifiCorp)
                        </option>

                        <option value="Wheatland City">
                            Wheatland City
                        </option>

                        <option value="Wheatland REA">
                            Wheatland REA
                        </option>
                    </select>