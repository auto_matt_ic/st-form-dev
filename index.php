<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script src="js/libraries/modernizr.js"></script>
        <!-- <script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script> -->

        <script src="//use.typekit.net/rez5fjc.js"></script>
        <script>try{Typekit.load();}catch(e){}</script>



         <link rel="stylesheet" type="text/css" href="css/st_form_styles.css">
         <link rel="stylesheet" type="text/css" href="css/main-tablet.css">
         <link rel="stylesheet" type="text/css" href="css/main-mobile.css">


        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    </head>
    <body>

    <div class="row row__top-bar">
    	<div class="main">
    		<a href="" class="branding solar-tribune">Solar Tribune</a>
    		<nav class="top-nav">
    			<!-- <ul>
    				<li>
    				</li>
    				<li>
    				</li>
    				<li>
    				</li>
    			</ul> -->
    		</nav><!--/.nav -->


    	</div><!--/.main -->


    </div><!--/.row -->

    <div class="row row__content">

    	<div class="main">


             <div class="form__slogan">
                <h1>Find the best deal on Solar</h1>
                <p>Learn why over 25,000 people compare quotes with us each month.</p>

            </div><!--/.form__slogan -->



    		<div class="form__wrapper">
              <?php include 'form.php'; ?>
	    	</div><!--/.form__wrapper -->






    	</div><!--/.main -->

    </div><!--/.content -->
    <div class="row row__footer">

    </div><!--/.footer -->



        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script> -->
        <script src="js/libraries/jquery.mask.js"></script>
        <script src="js/libraries/autofill-event.js"></script>
        <script src="js/libraries/TweenMax.min.js"></script>

        <script src="js/min/main-min.js"></script>



        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X');ga('send','pageview');
        </script>
    </body>
</html>