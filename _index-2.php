
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">

<title> | Solar TribuneSolar Tribune</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<link rel="stylesheet" type="text/css" href="../wp-content/themes/spectrum/style.css" media="screen" />
<link rel="stylesheet" type="text/css" media="all" href="../wp-content/themes/spectrum/css/effects.css" />
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://feeds.feedburner.com/SolarTribune" />
<link rel="pingback" href="http://solartribune.com/xmlrpc.php" />

<script type="text/javascript">
	window.google_analytics_uacct = "UA-16950466-6";
</script>

<!-- This site is optimized with the Yoast WordPress SEO plugin v1.1.9 - http://yoast.com/wordpress/seo/ -->
<link rel="canonical" href="index.html" />
<!-- / Yoast WordPress SEO plugin. -->


	<script type="text/javascript">//<![CDATA[
	// Google Analytics for WordPress by Yoast v4.2.3 | http://yoast.com/wordpress/google-analytics/
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount','UA-16950466-6']);
	_gaq.push(['_trackPageview']);
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
	//]]></script>
<link rel="alternate" type="application/rss+xml" title="Solar Tribune &raquo;  Comments Feed" href="http://solartribune.com/quote/feed/" />
<link rel='stylesheet' id='ois_reset-css'  href='../wp-content/plugins/OptinSkin/skins/css/ois_reset.css?ver=4.1' type='text/css' media='all' />
<link rel='stylesheet' id='ois_design_1-css'  href='../wp-content/plugins/OptinSkin/skins/css/design_2.css?ver=4.1' type='text/css' media='all' />
<link rel='stylesheet' id='ois_design_15-css'  href='../wp-content/plugins/OptinSkin/skins/css/design_1.css?ver=4.1' type='text/css' media='all' />
<link rel='stylesheet' id='ois_design_13-css'  href='../wp-content/plugins/OptinSkin/skins/css/design_13.css?ver=4.1' type='text/css' media='all' />
<link rel='stylesheet' id='simple_tooltips_style-css'  href='../wp-content/plugins/simple-tooltips/zebra_tooltips.css?ver=4.1' type='text/css' media='all' />
<link rel='stylesheet' id='output-css'  href='../wp-content/plugins/addthis/css/output.css?ver=4.1' type='text/css' media='all' />
<link rel='stylesheet' id='googleFont-css'  href='http://fonts.googleapis.com/css?family=Arial&#038;ver=4.1' type='text/css' media='all' />
<link rel='stylesheet' id='optinforms-stylesheet-css'  href='../wp-content/plugins/optin-forms/css/optinforms.css?ver=4.1' type='text/css' media='all' />
<link rel='stylesheet' id='alrp_slidebox.css-css'  href='../wp-content/plugins/seo-alrp/css/slidebox-light.css?ver=4.1' type='text/css' media='all' />
<link rel='stylesheet' id='ace_responsive-css'  href='../wp-content/plugins/simple-embed-code/css/ace-video-container.css?ver=4.1' type='text/css' media='all' />
<link rel='stylesheet' id='inbound-shortcodes-css'  href='../wp-content/plugins/cta/shared/shortcodes/css/frontend-render.css?ver=4.1' type='text/css' media='all' />
<link rel='stylesheet' id='google_font_Nobile-css'  href='http://fonts.googleapis.com/css?family=Nobile&#038;ver=4.1' type='text/css' media='all' />
<link rel='stylesheet' id='google_font_Open_Sans-css'  href='http://fonts.googleapis.com/css?family=Open+Sans&#038;ver=4.1' type='text/css' media='all' />
<link rel='stylesheet' id='google_font_Glegoo-css'  href='http://fonts.googleapis.com/css?family=Glegoo&#038;ver=4.1' type='text/css' media='all' />
<link rel='stylesheet' id='google_font_Cantarell-css'  href='http://fonts.googleapis.com/css?family=Cantarell&#038;ver=4.1' type='text/css' media='all' />
<link rel='stylesheet' id='tablepress-default-css'  href='../wp-content/plugins/tablepress/css/default.min.css?ver=1.0' type='text/css' media='all' />
<script type='text/javascript' src='../wp-includes/js/jquery/jquery.js?ver=1.11.1'></script>
<script type='text/javascript' src='../wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var inbound_settings = {"post_id":"4216","ip_address":"23.24.114.50","wp_lead_data":{"lead_id":null,"lead_email":null,"lead_uid":null},"admin_url":"http:\/\/solartribune.com\/wp-admin\/admin-ajax.php","track_time":"2015\/01\/25 19:24:49","post_type":"page","page_tracking":"on","search_tracking":"on","comment_tracking":"on","custom_mapping":[],"inbound_track_exclude":"","inbound_track_include":""};
/* ]]> */
</script>
<script type='text/javascript' src='../wp-content/plugins/cta/shared/assets/frontend/js/analytics/inboundAnalytics.js?ver=4.1'></script>
<script type='text/javascript' src='../wp-content/plugins/optin-forms/js/placeholder.js?ver=4.1'></script>
<script type='text/javascript' src='../wp-content/plugins/seo-alrp/js/slidebox.js?ver=4.1'></script>
<script type='text/javascript' src='../wp-content/plugins/wp-float/js/jquery.easing.js?ver=4.1'></script>
<script type='text/javascript' src='../wp-content/plugins/wp-float/js/jquery.floater.2.2.js?ver=4.1'></script>
<script type='text/javascript' src='../wp-content/plugins/wp-float/js/jquery.hoverIntent.minified.js?ver=4.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var ois = {"ajaxurl":"http:\/\/solartribune.com\/wp-admin\/admin-ajax.php","ois_submission_nonce":"15f8be5d6f","disable_submissions_stats":"no","disable_impressions_stats":"no"};
/* ]]> */
</script>
<script type='text/javascript' src='../wp-content/plugins/OptinSkin/front/js/optin.js?ver=4.1'></script>
<script type='text/javascript' src='../wp-content/plugins/OptinSkin/front/js/jquery.simplemodal.js?ver=4.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var ois_fade = {"ois_2":"1","ois_3":"1","ois_4":"1","ois_5":"10","ois_7":"3","ois_9":"3","ois_10":"3","ois_12":"1","ois_13":"1","ois_14":"1","ois_15":"1","ois_16":"1","ois_17":"1","ois_19":"3"};
/* ]]> */
</script>
<script type='text/javascript' src='../wp-content/plugins/OptinSkin/front/js/fade_load.js?ver=4.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var ois_stick = {"ois_5":"sticker","ois_7":"sticker","ois_9":"sticker","ois_10":"sticker"};
/* ]]> */
</script>
<script type='text/javascript' src='../wp-content/plugins/OptinSkin/front/js/sticky.js?ver=4.1'></script>
<script type='text/javascript' src='../wp-content/plugins/OptinSkin/front/includes/waypoints.min.js?ver=4.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var cta_variation = {"cta_id":null,"ajax_url":"http:\/\/solartribune.com\/wp-admin\/admin-ajax.php","admin_url":"http:\/\/solartribune.com\/wp-admin\/admin-ajax.php","home_url":"http:\/\/solartribune.com","disable_ajax":"0"};
/* ]]> */
</script>
<script type='text/javascript' src='../wp-content/plugins/cta/js/cta-load-variation.js?ver=1'></script>
<script type='text/javascript' src='../wp-content/plugins/cta/shared/assets/global/js/jquery.cookie.js?ver=4.1'></script>
<script type='text/javascript' src='../wp-content/plugins/cta/shared/assets/global/js/jquery.total-storage.min.js?ver=4.1'></script>
<script type='text/javascript' src='../wp-content/themes/spectrum/includes/js/superfish.js?ver=4.1'></script>
<script type='text/javascript' src='../wp-content/themes/spectrum/includes/js/woo_tabs.js?ver=4.1'></script>
<script type='text/javascript' src='../wp-content/themes/spectrum/includes/js/general.js?ver=4.1'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://solartribune.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://solartribune.com/wp-includes/wlwmanifest.xml" />
<meta name="generator" content="WordPress 4.1" />
<link rel='shortlink' href='http://solartribune.com/?p=4216' />
<style type="text/css">
#alrp-related-posts h3, #alrp-related-posts h2{font-size:1.2em;font-style:normal;margin-bottom: 10px;}
.alrp-thumbnail img { background-color:#fff; float:left; margin:0 10px 0 0; padding:7px; }
.alrp-content { min-height:101px; margin-bottom:5px; }
.alrp-content a { text-decoration:none; }
alrp-content p { padding-bottom:5px; }
</style><link rel="stylesheet" type="text/css" href="../wp-content/plugins/social-media-widget/social_widget.css" />

<!-- Theme version -->
<meta name="generator" content="Spectrum News 1.3.2" />
<meta name="generator" content="WooFramework 5.5.6" />

<!-- Alt Stylesheet -->
<link href="../wp-content/themes/spectrum/styles/default.css" rel="stylesheet" type="text/css" />

<!-- Custom Favicon -->
<link rel="shortcut icon" href="../wp-content/uploads/2012/09/fb_logo_v3-2.png"/>

<!-- Woo Shortcodes CSS -->
<link href="../wp-content/themes/spectrum/functions/css/shortcodes.css" rel="stylesheet" type="text/css" />

<!-- Custom Stylesheet -->
<link href="../wp-content/themes/spectrum/custom.css" rel="stylesheet" type="text/css" />

<!--[if IE 6]>
<script type="text/javascript" src="http://solartribune.com/wp-content/themes/spectrum/includes/js/pngfix.js"></script>
<script type="text/javascript" src="http://solartribune.com/wp-content/themes/spectrum/includes/js/menu.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="http://solartribune.com/wp-content/themes/spectrum/css/ie6.css" />
<![endif]-->

<!--[if IE 7]>
<link rel="stylesheet" type="text/css" media="all" href="http://solartribune.com/wp-content/themes/spectrum/css/ie7.css" />
<![endif]-->

<!--[if IE 8]>
<link rel="stylesheet" type="text/css" media="all" href="http://solartribune.com/wp-content/themes/spectrum/css/ie8.css" />
<![endif]-->

<script>(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script><div id="fb-root"></div><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script><script type="text/javascript">
			  (function() {
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script><!-- Woo Custom Styling -->
<style type="text/css">
body {background-color:#ffffff}
a:link, a:visited {color:#7474fc}
a:hover {color:#fabc03}
.button, .reply a {background-color:#f2f2f2}
#recent-posts .post {height:90px}
</style>


</head>

<body class="page page-id-4216 page-parent page-template page-template-template-fullwidth page-template-template-fullwidth-php unknown alt-style-default">


<div id="wrapper">
<div id="background">

	<div id="top-nav" class="col-full">
		<ul id="menu-top-mini-menu" class="nav"><li id="menu-item-3806" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-3806"><a href="http://solartribune.com/" >Solar Energy News</a></li>
<li id="menu-item-4230" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4230"><a href="http://solartribune.com/your-home/" >Home Solar Panels</a></li>
<li id="menu-item-4555" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4555"><a href="http://solartribune.com/advertise/" >Advertise with us</a></li>
</ul>
       <span class="nav-item-right"><a href="http://feeds.feedburner.com/SolarTribune"><img src="../wp-content/themes/spectrum/images/ico-rss.png" alt="RSS Feed" /></a></span>
	</div><!-- /#top-nav -->

	<div id="header" class="col-full">

		<div id="logo">

		            <a href="http://solartribune.com" title="Solar Energy News, Analysis, Education">
                <img src="../wp-content/uploads/2012/09/solar_tribune_header_03-e1348012219201.png" alt="Solar Tribune" />
            </a>


                    <span class="site-title"><a href="http://solartribune.com">Solar Tribune</a></span>
                    <span class="site-description">Solar Energy News, Analysis, Education</span>

		</div><!-- /#logo -->


	</div><!-- /#header -->

	<div id="main-nav" class="col-full">
		<ul id="menu-primary-menu" class="nav"><li id="menu-item-1532" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-has-children menu-item-1532"><a title="Solar Energy News" href="http://solartribune.com/" >Solar News</a>
<ul class="sub-menu">
	<li id="menu-item-7477" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-7477"><a href="http://solartribune.com/category/solar-news/new-solar-technology/" >Technology</a></li>
	<li id="menu-item-7476" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-7476"><a title="Solar Policy News" href="http://solartribune.com/category/solar-news/solar-legislation/" >Policy</a></li>
</ul>
</li>
<li id="menu-item-1477" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-1477"><a title="Solar PV Panels for Homes" href="http://solartribune.com/your-home/" >Solar Panels</a>
<ul class="sub-menu">
	<li id="menu-item-7412" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-7412"><a href="http://solartribune.com/solar-panel-placement/" >Evaluating Your Home</a>
	<ul class="sub-menu">
		<li id="menu-item-7445" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7445"><a href="http://solartribune.com/how-many-solar-panels-do-i-need/" >How Many Panels?</a></li>
		<li id="menu-item-7446" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7446"><a href="http://solartribune.com/off-grid-solar-systems/" >On or Off Grid?</a></li>
	</ul>
</li>
	<li id="menu-item-7413" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-7413"><a href="http://solartribune.com/home-solar-energy-system/" >System Components</a>
	<ul class="sub-menu">
		<li id="menu-item-7414" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-7414"><a href="http://solartribune.com/types-of-solar-panels/" >Panel Types</a>
		<ul class="sub-menu">
			<li id="menu-item-7458" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7458"><a href="http://solartribune.com/monocrystalline-solar-cells/" >Monocrystalline Panels</a></li>
			<li id="menu-item-7459" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7459"><a href="http://solartribune.com/polycrystalline-solar-cells/" >Polycrystalline Panels</a></li>
			<li id="menu-item-7460" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7460"><a href="http://solartribune.com/thin-film-photovoltaic/" >Thin Film &#038; Shingles</a></li>
		</ul>
</li>
		<li id="menu-item-7418" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7418"><a href="http://solartribune.com/solar-panel-inverters/" >Inverters</a></li>
		<li id="menu-item-7467" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7467"><a href="http://solartribune.com/solar-charge-controllers/" >Charge Controllers</a></li>
		<li id="menu-item-7466" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7466"><a href="http://solartribune.com/solar-panels-batteries/" >Batteries</a></li>
	</ul>
</li>
	<li id="menu-item-7415" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-7415"><a href="http://solartribune.com/financing-solar/" >Financing</a>
	<ul class="sub-menu">
		<li id="menu-item-7469" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7469"><a href="http://solartribune.com/ppa-solar/" >PPAs</a></li>
		<li id="menu-item-7451" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7451"><a href="http://solartribune.com/leasing-solar-panel/" >Leasing</a></li>
		<li id="menu-item-7470" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7470"><a href="http://solartribune.com/feed-in-tariff-solar/" >Feed in Tariff</a></li>
		<li id="menu-item-7468" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7468"><a href="http://solartribune.com/solar-panels-tax-credit/" >Tax Credits</a></li>
	</ul>
</li>
	<li id="menu-item-7597" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-7597"><a href="http://solartribune.com/your-home/solar-companies/" >Companies</a>
	<ul class="sub-menu">
		<li id="menu-item-7595" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-7595"><a href="http://solartribune.com/your-home/solar-companies/california/" >California</a>
		<ul class="sub-menu">
			<li id="menu-item-7723" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7723"><a href="http://solartribune.com/your-home/solar-companies/california/bakersfield/" >Bakersfield</a></li>
			<li id="menu-item-7601" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7601"><a href="http://solartribune.com/your-home/solar-companies/california/berkeley/" >Berkeley</a></li>
			<li id="menu-item-7594" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7594"><a href="http://solartribune.com/your-home/solar-companies/california/concord/" >Concord</a></li>
			<li id="menu-item-7721" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7721"><a href="http://solartribune.com/your-home/solar-companies/california/fresno/" >Fresno</a></li>
			<li id="menu-item-7593" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7593"><a href="http://solartribune.com/your-home/solar-companies/california/hayward/" >Hayward</a></li>
			<li id="menu-item-8428" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8428"><a href="http://solartribune.com/your-home/solar-companies/california/long-beach/" >Long Beach</a></li>
			<li id="menu-item-7592" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7592"><a href="http://solartribune.com/your-home/solar-companies/california/los-angeles/" >Los Angeles</a></li>
			<li id="menu-item-7591" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7591"><a href="http://solartribune.com/your-home/solar-companies/california/modesto/" >Modesto</a></li>
			<li id="menu-item-7590" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7590"><a href="http://solartribune.com/your-home/solar-companies/california/orange/" >Orange</a></li>
			<li id="menu-item-7589" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7589"><a href="http://solartribune.com/your-home/solar-companies/california/riverside/" >Riverside</a></li>
			<li id="menu-item-7588" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7588"><a href="http://solartribune.com/your-home/solar-companies/california/sacramento/" >Sacramento</a></li>
			<li id="menu-item-7587" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7587"><a href="http://solartribune.com/your-home/solar-companies/california/san-diego/" >San Diego</a></li>
			<li id="menu-item-7586" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7586"><a href="http://solartribune.com/your-home/solar-companies/california/san-francisco/" >San Francisco</a></li>
			<li id="menu-item-7585" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7585"><a href="http://solartribune.com/your-home/solar-companies/california/san-jose/" >San Jose</a></li>
			<li id="menu-item-7661" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-7661"><a href="http://solartribune.com/your-home/solar-companies/california/" >More</a>
			<ul class="sub-menu">
				<li id="menu-item-7651" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7651"><a href="http://solartribune.com/your-home/solar-companies/california/cathedral-city/" >Cathedral City</a></li>
				<li id="menu-item-7652" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7652"><a href="http://solartribune.com/your-home/solar-companies/california/camarillo/" >Camarillo</a></li>
				<li id="menu-item-7653" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7653"><a href="http://solartribune.com/your-home/solar-companies/california/moreno-valley/" >Moreno Valley</a></li>
				<li id="menu-item-7654" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7654"><a href="http://solartribune.com/your-home/solar-companies/california/highland/" >Highland California</a></li>
				<li id="menu-item-7655" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7655"><a href="http://solartribune.com/your-home/solar-companies/california/livermore/" >Livermore</a></li>
				<li id="menu-item-7656" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7656"><a href="http://solartribune.com/your-home/solar-companies/california/thousand-oaks/" >Thousand Oaks</a></li>
				<li id="menu-item-7657" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7657"><a href="http://solartribune.com/your-home/solar-companies/california/poway/" >Poway</a></li>
				<li id="menu-item-7650" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7650"><a href="http://solartribune.com/your-home/solar-companies/california/davis/" >Davis</a></li>
			</ul>
</li>
		</ul>
</li>
		<li id="menu-item-7990" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7990"><a href="http://solartribune.com/your-home/solar-companies/washington-dc/" >Washington DC</a></li>
		<li id="menu-item-7992" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7992"><a href="http://solartribune.com/your-home/solar-companies/delaware/wilmington/" >Wilmington DE</a></li>
		<li id="menu-item-8070" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8070"><a href="http://solartribune.com/your-home/solar-companies/arizona/tucson/" >Tucson AZ</a></li>
		<li id="menu-item-8071" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8071"><a href="http://solartribune.com/your-home/solar-companies/arizona/phoenix/" >Phoenix AZ</a></li>
		<li id="menu-item-7991" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7991"><a href="http://solartribune.com/your-home/solar-companies/california/hawaii/" >Hawaii</a></li>
	</ul>
</li>
	<li id="menu-item-7450" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-7450"><a href="http://solartribune.com/frequently-asked-home-solar-questions/" >FAQ</a>
	<ul class="sub-menu">
		<li id="menu-item-7452" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7452"><a href="http://solartribune.com/solar-power-measurement/" >Measuring Solar Energy</a></li>
		<li id="menu-item-7453" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7453"><a href="http://solartribune.com/how-much-are-solar-panels/" >Is Solar Expensive?</a></li>
		<li id="menu-item-7454" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7454"><a href="http://solartribune.com/solar-panel-payback/" >Solar Payback Period</a></li>
		<li id="menu-item-7456" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7456"><a href="http://solartribune.com/solar-panels-efficiency/" >Solar Efficiency</a></li>
		<li id="menu-item-7455" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7455"><a href="http://solartribune.com/how-much-are-solar-panels/" >What Impacts the Price?</a></li>
		<li id="menu-item-7457" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7457"><a href="http://solartribune.com/how-long-do-solar-cells-last/" >Lifetime</a></li>
	</ul>
</li>
	<li id="menu-item-7416" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7416"><a href="http://solartribune.com/3-ways-to-save-thousands-on-home-solar-panels/" >Tips for Saving Money</a></li>
	<li id="menu-item-7417" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item menu-item-7417"><a href="index.html" >Get a Quote</a></li>
</ul>
</li>
<li id="menu-item-1484" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-1484"><a title="Solar Thermal for Homes" href="http://solartribune.com/solar-thermal-power/" >Solar Thermal</a>
<ul class="sub-menu">
	<li id="menu-item-7480" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-7480"><a href="http://solartribune.com/solar-hot-water-panels/" >Solar Water Heating</a>
	<ul class="sub-menu">
		<li id="menu-item-7481" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-7481"><a href="http://solartribune.com/solar-hot-water-parts/" >Parts</a>
		<ul class="sub-menu">
			<li id="menu-item-7506" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7506"><a href="http://solartribune.com/solar-thermal-collectors/" >Solar Collectors</a></li>
			<li id="menu-item-7513" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7513"><a href="http://solartribune.com/solar-storage/" >Water Storage Tanks</a></li>
			<li id="menu-item-7514" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7514"><a href="http://solartribune.com/thermal-transfer-fluid/" >Transfer Fluid</a></li>
			<li id="menu-item-7515" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7515"><a href="http://solartribune.com/what-is-a-heat-exchanger/" >Heat Exchanger</a></li>
		</ul>
</li>
		<li id="menu-item-7507" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-7507"><a href="http://solartribune.com/solar-thermal-collectors/" >Solar Collector Types</a>
		<ul class="sub-menu">
			<li id="menu-item-7510" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7510"><a href="http://solartribune.com/batch-solar-water-heater/" >Batch</a></li>
			<li id="menu-item-7508" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7508"><a href="http://solartribune.com/evacuated-tube-solar-hot-water/" >Evacuated Tube</a></li>
			<li id="menu-item-7509" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7509"><a href="http://solartribune.com/solar-flat-plate-collector/" >Flat Plate</a></li>
			<li id="menu-item-7511" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7511"><a href="http://solartribune.com/thermosiphon-solar/" >Thermosiphon</a></li>
		</ul>
</li>
		<li id="menu-item-7512" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7512"><a href="http://solartribune.com/what-size-water-heater/" >What Size?</a></li>
	</ul>
</li>
	<li id="menu-item-7479" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7479"><a href="http://solartribune.com/solar-thermal-heating/" >Home Heating</a></li>
	<li id="menu-item-7482" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7482"><a href="http://solartribune.com/solar-pool-heater/" >Pool Heating</a></li>
	<li id="menu-item-7483" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item menu-item-7483"><a href="http://solartribune.com/quote" >Get a Quote</a></li>
</ul>
</li>
<li id="menu-item-1483" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1483"><a title="Solar Facts" href="http://solartribune.com/what-is-solar-power/" >The Environment</a>
<ul class="sub-menu">
	<li id="menu-item-7471" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7471"><a href="http://solartribune.com/how-to-store-solar-energy/" >Storing Solar Energy</a></li>
	<li id="menu-item-7472" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7472"><a href="http://solartribune.com/solar-farms/" >Solar Farms</a></li>
	<li id="menu-item-7473" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7473"><a href="http://solartribune.com/solar-energy-disadvantages/" >Advantages &#038; Disadvantages</a></li>
	<li id="menu-item-7474" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7474"><a href="http://solartribune.com/history-of-photovoltaics/" >History</a></li>
</ul>
</li>
<li id="menu-item-1487" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1487"><a href="http://solartribune.com/about-us/" >About</a>
<ul class="sub-menu">
	<li id="menu-item-4573" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4573"><a href="http://solartribune.com/advertise/" >Advertising</a></li>
	<li id="menu-item-5792" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5792"><a href="http://solartribune.com/internship/" >Part-Time Digital Marketing Internship &#8211; Washington DC</a></li>
	<li id="menu-item-4733" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4733"><a href="http://solartribune.com/site-map/" >Site Map</a></li>
	<li id="menu-item-3805" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3805"><a href="http://solartribune.com/about-us/submit-your-business/" >Get Your Business Listed on Solar Tribune</a></li>
</ul>
</li>
</ul>
	</div><!-- /#main-nav -->

    <div id="content" class="page col-full">
		<div id="main" class="fullwidth">


                <div class="post">

                    <h1 class="title"><a href="index.html" rel="bookmark" title=""></a></h1>

                    <div class="entry">
	                	<div id="compare_quotes_container">
<div id="iframe_container">
<p>Compare Quotes from Companies Near You</p>
<div id="iframe_container_inner">
<p>


	<!-- <iframe src="http://widgets.solaramerica.org/lurl/signup-for-free-quotes/?campid=7DB672BB80F7CF60" width="320" height="240" scrolling="no">
	</iframe> -->

<?php include "new_form.php" ?>

</p>
</div>
</div>
<h1>Find the best deal on Solar</h1>
<h2>Learn why over <span style="color: #445f8b;">25,000</span><br />
people compare quotes<br />
with us each month.</h2>
<ul>
<li id="first_step">Fill out our form</li>
<li id="second_step">Compare quotes</li>
</ul>
</div>
<div id="partner_container">
<p id="partner_text">In Partnership with:</p>
<ul>
<li><a id="solar_america_partner" href="http://www.solaramerica.org/" onclick="javascript:_gaq.push(['_trackEvent','outbound-article','http://www.solaramerica.org']);" target="_blank"></a></li>
<li><a id="dept_of_energy_partner" href="http://www.energy.gov/" onclick="javascript:_gaq.push(['_trackEvent','outbound-article','http://www.energy.gov']);" target="_blank"></a></li>
<li><a id="bbb_partner" href="http://www.bbb.org/" onclick="javascript:_gaq.push(['_trackEvent','outbound-article','http://www.bbb.org']);" target="_blank"></a></li>
</ul>
</div>
<p><!-- Facebook Conversion Code for Quote --><br />
<script>// <![CDATA[
(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6015566358101', {'value':'0.01','currency':'USD'}]);
// ]]&gt;</script><br />
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6015566358101&amp;cd[value]=0.01&amp;cd[currency]=USD&amp;noscript=1" /></noscript></p>
<div id="alrp-slidebox-anchor"></div>	               	</div><!-- /.entry -->

                </div><!-- /.post -->



		</div><!-- /#main -->

    </div><!-- /#content -->
</div>
</div>
		<div id="footer-widgets">

		<div class="col-full">

			<div class="block">
        		<div id="text-35" class="widget widget_text">			<div class="textwidget"><a href="http://solartibune.com/privacy/" onclick="javascript:_gaq.push(['_trackEvent','outbound-widget','http://solartibune.com']);">Privacy Policy</a></div>
		</div>
			</div>
			<div class="block">

			</div>
			<div class="block">

			</div>
			<div class="block last">

			</div>

			<div class="fix"></div>

		</div><!-- /.col-full -->

	</div><!-- /#footer-widgets  -->

	<div id="footer">

		<div class="inner">

			<div id="credits" class="col-left">

					<a href="http://solartribune.com" title="Solar Energy News, Analysis, Education">
													<img class="footer-logo" src="../wp-content/uploads/2012/09/solar_tribune_header_031-300x68.png" alt="Footer logo" />
											</a>



			    <p>&copy; 2015 Solar Tribune. All Rights Reserved.<br />

            			</div>

			<div id="footer-search" class="col-right">

            				    <form method="get" class="searchform" action="http://solartribune.com" >
        <input type="text" class="field s" name="s" value="Search..." onfocus="if (this.value == 'Search...') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Search...';}" />
        <input type="submit" class="submit button" name="submit" value="Search" />
        <div class="fix"></div>
    </form>


			</div><!-- /#footer-search -->

		</div><!-- /.inner -->

		<div class="fix"></div>

	</div><!-- /#footer  -->

</div><!-- /#background -->

</div><!-- /#wrapper -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
  _fbq.push(['addPixelId', '307069002785612']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=307069002785612&amp;ev=NoScript" /></noscript>


<!-- Google Code for Main Tag? -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 968939569;
var google_conversion_label = "o9_oCMe6uQQQsbCDzgM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/968939569/?value=0&amp;label=o9_oCMe6uQQQsbCDzgM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<script type="text/javascript">
adroll_adv_id = "36VYNSIM7NGZTMOAPM3MV7";
adroll_pix_id = "A24ULUUS2BDCHNQBD7MIZG";
(function () {
var oldonload = window.onload;
window.onload = function(){
   __adroll_loaded=true;
   var scr = document.createElement("script");
   var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
   scr.setAttribute('async', 'true');
   scr.type = "text/javascript";
   scr.src = host + "/j/roundtrip.js";
   ((document.getElementsByTagName('head') || [null])[0] ||
    document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
   if(oldonload){oldonload()}};
}());
</script>

<!-- HitTail Code -->
<script type="text/javascript">
	(function(){ var ht = document.createElement('script');ht.async = true;
	  ht.type='text/javascript';ht.src = '//103288.hittail.com/mlt.js';
	  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ht, s);})();
</script>
<!-- Woo Tabs Widget -->
<script type="text/javascript">
jQuery(document).ready(function(){
	// UL = .wooTabs
	// Tab contents = .inside

	var tag_cloud_class = '#tagcloud';

	//Fix for tag clouds - unexpected height before .hide()
	var tag_cloud_height = jQuery('#tagcloud').height();

	jQuery('.inside ul li:last-child').css('border-bottom','0px'); // remove last border-bottom from list in tab content
	jQuery('.wooTabs').each(function(){
		jQuery(this).children('li').children('a:first').addClass('selected'); // Add .selected class to first tab on load
	});
	jQuery('.inside > *').hide();
	jQuery('.inside > *:first-child').show();

	jQuery('.wooTabs li a').click(function(evt){ // Init Click funtion on Tabs

		var clicked_tab_ref = jQuery(this).attr('href'); // Strore Href value

		jQuery(this).parent().parent().children('li').children('a').removeClass('selected'); //Remove selected from all tabs
		jQuery(this).addClass('selected');
		jQuery(this).parent().parent().parent().children('.inside').children('*').hide();

		jQuery('.inside ' + clicked_tab_ref).fadeIn(500);

		 evt.preventDefault();

	})
})
</script>
<script type="text/javascript">if (typeof(addthis_share) == "undefined"){ addthis_share = 1;}

var addthis_config = {"data_track_clickback":true,"data_ga_property":"UA-24288935-1","data_ga_social":true,"data_track_addressbar":true,"data_track_textcopy":false,"ui_atversion":"300"};
var addthis_product = 'wpp-3.0.5';
</script><script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f49b1492a7ba262"></script><script type='text/javascript' src='../wp-content/plugins/simple-tooltips/zebra_tooltips.js?ver=4.1'></script>
<script type='text/javascript' src='../wp-includes/js/comment-reply.min.js?ver=4.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var cta_reveal = {"admin_url":"http:\/\/solartribune.com\/wp-admin\/admin-ajax.php","home_url":"http:\/\/solartribune.com"};
/* ]]> */
</script>
<script type='text/javascript' src='../wp-content/plugins/cta/js/cta-reveal-variation.js?ver=1'></script>
        			<script type="text/javascript">
				jQuery(function() {

					new jQuery.Zebra_Tooltips(jQuery('.tooltips'), {
						'background_color':     '#000000',
						'color':				'#ffffff',
						'max_width':  250,
						'opacity':    .75,
						'position':    'center'
					});
				});
            </script>
        <!--wp_footer--></body>
</html>